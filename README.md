# README #

### What is this repository for? ###

This is a document-based macOS app. It is meant for musicians that want to create visually appealing chord charts.
Each section of the chart(i.e. Verses and Choruses) is defined using ChordPro.

### Who do I talk to? ###

Joseph Van Boxtel
dev.josephvanboxtel@gmail.com