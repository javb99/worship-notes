//
//  ChartDocument.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 1/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa
import Quartz

class ChartDocument: NSDocument {
    
    private(set) var song: Song
    
    var songSequence: [Section] {
        return song.sequence
    }
    
    var songTitle: String {
        set {
            song.meta.title = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.title, newValue: newValue)
            }
        }
        get {
            return song.meta.title
        }
    }
    
    var songAuthors: String? {
        set {
            song.meta.authors = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.authors, newValue: newValue)
            }
        }
        get {
            return song.meta.authors
        }
    }
    
    var songKey: MusicalNote {
        set {
            song.meta.key = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.key, newValue: newValue)
            }
        }
        get {
            return song.meta.key
        }
    }
    
    /// The tempo of the song in Beats Per Minute(BPM).
    var songTempo: Double? {
        set {
            song.meta.tempo = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.tempo, newValue: newValue)
            }
        }
        get {
            return song.meta.tempo
        }
    }
    
    /// The time signature of the song.
    var songMeasure: String? {
        set {
            song.meta.measure = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.measure, newValue: newValue)
            }
        }
        get {
            return song.meta.measure
        }
    }
    
    /// The copyright information for the song. This is usually multiple lines.
    var songCopyright: String? {
        set {
            song.meta.copyright = newValue
            for observer in songObservers {
                observer.song(song, didChangeMetaField: \SongMeta.copyright, newValue: newValue)
            }
        }
        get {
            return song.meta.copyright
        }
    }
    
    var chartWindowController: ChartWindowController {
        return windowControllers.first! as! ChartWindowController
    }
    
    /// Objects that want to be notified anytime the song model changes.
    var songObservers: [SongObserver] = []
    
    /// An object that manages the exporting of this document to a PDF.
    var exporter: ChartDocumentExporter
    
    override init() {
        song = Song(meta: SongMeta(title: "Untitled", authors: nil, key: .c, tempo: nil, measure: nil, copyright: nil), sequence: [])
        exporter = ChartDocumentExporter()
        super.init()
    }
    
    override class var autosavesInPlace: Bool {
        return true
    }
    
    override func makeWindowControllers() {
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        let windowController = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("Song Window")) as! ChartWindowController
        let chartViewingController = windowController.contentViewController?.children[1] as! ChartViewingController
        let chartInfoVC = windowController.contentViewController?.children[0].children[0] as! ChartInfoCreationController
        let chartSectionVC = windowController.contentViewController?.children[0].children[1] as! ChartSectionCreationController

        songObservers.append(chartViewingController)
        songObservers.append(chartInfoVC)
        songObservers.append(chartSectionVC)
        chartInfoVC.document = self
        chartSectionVC.document = self
        
        self.addWindowController(windowController)
    }
    
    override func data(ofType typeName: String) throws -> Data {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        do {
            let data = try encoder.encode(song)
            return data
        } catch {
            // TODO: Create custom error
            throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
        }
    }
    
    override func read(from data: Data, ofType typeName: String) throws {
        let decoder = PropertyListDecoder()
        do {
            song = try decoder.decode(Song.self, from: data)
        } catch {
            // TODO: Create custom error
            throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
        }
    }
}

//
// MARK: - Printing and PDFs -
//

extension ChartDocument {
    
    @objc func exportDocument(_ sender: Any?) {
        exporter.export(self)
    }
    
    override func printOperation(withSettings printSettings: [NSPrintInfo.AttributeKey : Any]) throws -> NSPrintOperation {
        let printView = exporter.createPrintableChartView(for: song)
        let operation = NSPrintOperation(view: printView)
        return operation
    }
}

//
// MARK: - Song setters and observation -
//
extension ChartDocument {
    func insert(_ section: Section, atIndex index: Int) {
        // TODO: Add guard statements
        song.sequence.insert(section, at: index)
        for observer in songObservers {
            observer.song(song, didInsert: section, atIndex: index)
        }
    }
    
    func append(_ section: Section) {
        // TODO: Add guard statements
        let index = song.sequence.endIndex
        song.sequence.insert(section, at: index)
        for observer in songObservers {
            observer.song(song, didInsert: section, atIndex: index)
        }
    }
    
    func removeSection(atIndex index: Int) {
        // TODO: Add guard statements
        song.sequence.remove(at: index)
        for observer in songObservers {
            observer.song(song, didRemoveSectionAtIndex: index)
        }
    }
    
    func replaceSection(atIndex index: Int, with section: Section) {
        // TODO: Add guard statements
        song.sequence[index] = section
        for observer in songObservers {
            observer.song(song, didReplaceSectionAtIndex: index, with: section)
        }
    }
    
    func move(_ section: Section, fromIndex: Int, toIndex: Int) {
        // TODO: Add guard statements
        var adjustedToIndex = toIndex
        
        // If the item is being moved toward the back of the list, adjust the index forward to compensate.
        if toIndex > fromIndex {
            adjustedToIndex -= 1
        }
        
        song.sequence.remove(at: fromIndex)
        song.sequence.insert(section, at: adjustedToIndex)
        
        for observer in songObservers {
            observer.song(song, didMove: section, fromIndex: fromIndex, toIndex: adjustedToIndex)
        }
    }
}

protocol SongObserver {
    
    func songBeginBatchUpdates(_ song: Song)
    
    func songEndBatchUpdates(_ song: Song)
    
    func song(_ song: Song, didInsert section: Section, atIndex index: Int)
    
    func song(_ song: Song, didRemoveSectionAtIndex index: Int)
    
    func song(_ song: Song, didReplaceSectionAtIndex index: Int, with section: Section)
    
    /// From index is the index the item was removed from. The toIndex is where the item was inserted. The toIndex was altered if the item was moved toward the end of the sequence.
    func song(_ song: Song, didMove section: Section, fromIndex: Int, toIndex: Int)
    
    /// Title, author, key, tempo, or time signature of the song has changed.
    func song<Value>(_ song: Song, didChangeMetaField field: KeyPath<SongMeta, Value>, newValue: Value)
}

extension SongObserver {
    func songBeginBatchUpdates(_ song: Song) {}
    func songEndBatchUpdates(_ song: Song) {}
    
    func song(_ song: Song, didInsert section: Section, atIndex index: Int) {}
    
    func song(_ song: Song, didRemoveSectionAtIndex index: Int) {}
    
    func song(_ song: Song, didReplaceSectionAtIndex index: Int, with section: Section) {}

    func song(_ song: Song, didMove section: Section, fromIndex: Int, toIndex: Int) {}
    
    func song<Value>(_ song: Song, didChangeMetaField field: KeyPath<SongMeta, Value>, newValue: Value) {}
}
