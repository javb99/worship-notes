//
//  ChartWindowController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 1/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class ChartWindowController: NSWindowController {
    
    @IBOutlet weak var zoomPopUpButton: NSPopUpButton!
    
    var zoomManager: ZoomManager!
    
    @IBOutlet weak var viewingKeyButton: NSPopUpButton!
    
    var chartViewingController: ChartViewingController? {
        return self.contentViewController?.children[1] as? ChartViewingController
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        shouldCascadeWindows = true
    }
    
    override func windowDidLoad() {
        print("Chart Window Controller - Window Did Load")
        zoomManager = ZoomManager(scrollView: chartViewingController!.scrollView, popup: zoomPopUpButton)
        viewingKeyButton.target = chartViewingController
        viewingKeyButton.action = #selector(ChartViewingController.viewingKeyDidChange(_:))
    }
}
