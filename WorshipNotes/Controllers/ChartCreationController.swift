//
//  ChartCreationController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/13/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSStoryboard.Name {
    static let chartCreation = NSStoryboard.Name("ChartCreation")
}

class ChartCreationController: NSTabViewController {
    
}
