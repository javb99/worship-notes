//
//  ZoomManager.swift
//  DocEx
//
//  Created by Joseph Van Boxtel on 1/26/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

/// A class that links a NSPopupButton with a menu filled with zoom values and modes to a FittingScrollView that can zoom according to them.
public class ZoomManager: ScrollViewLiveUpdatesDelegate {
    
    /// The FittingScrollView that is being zoomed.
    private(set) public var scrollView: FittingScrollView
    
    /// The the items in the menu of this popUpButton use the tag to set the magnification. The tag is interpretted as the magnification level as a percentage. Negative tags are used for different modes, currently fit to page and fit to height. See fitHeightIdentifier and fitPageIdentifier.
    private(set) public var zoomPopUpButton: NSPopUpButton
    
    /// The tag value used for the menu item that turns on 'Fit Height'.
    static let fitHeightIdentifier = -1
    
    /// The tag value used for the menu item that turns on 'Fit Page'.
    static let fitPageIdentifier = -2
    
    public init(scrollView: FittingScrollView, popup: NSPopUpButton) {
        self.scrollView = scrollView
        self.zoomPopUpButton = popup
        
        for item in zoomPopUpButton.menu!.items {
            item.target = self
            switch item.tag {
            case ZoomManager.fitHeightIdentifier:
                item.action = #selector(selectFitHeight(_:))
            case ZoomManager.fitPageIdentifier:
                item.action = #selector(selectFitPage(_:))
            default:
                item.action = #selector(menuItemPicked(_:))
            }
        }
        
        scrollView.liveUpdatesDelegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(endedLiveMagnify(_:)), name: NSScrollView.didEndLiveMagnifyNotification, object: scrollView)
        
        scrollView.documentFitDimension = .height

        updateMagnificationTitle()
    }
    
    /// Updates the title of the popup button to the current magnification percentage. The magnification is clipped to the min and max magnification of the scrollView.
    public func updateMagnificationTitle() {
        // Clip magnification to bounds
        let magnification = max(min(scrollView.magnification, scrollView.maxMagnification), scrollView.minMagnification)
        let roundedMag = Int(round(magnification*100.0))
        zoomPopUpButton.cell?.title = "\(roundedMag)%"
    }
    
    public func scrollView(_ scrollView: FittingScrollView, magnificationChanged magnification: CGFloat) {
        updateMagnificationTitle()
    }
    
    /// Sent by the scrollView when a live magnify gesture ends.
    @objc private func endedLiveMagnify(_ notification: Notification) {
        zoomPopUpButton.menu?.deselectAllItems()
        updateMagnificationTitle()
    }
    
    /// An item in the menu has been selected. Use its tag to set the magnification level of the scrollView.
    @objc private func menuItemPicked(_ menuItem: NSMenuItem) {
        menuItem.menu?.deselectAllItems()
        menuItem.state = .on
       
        scrollView.documentFitDimension = .none
        scrollView.animator().magnification = CGFloat(menuItem.tag) / 100
        updateMagnificationTitle()
    }
    
    /// The Fit Height menu button has been pressed. Set the scrollView's documentFitDimension to height.
    @objc private func selectFitHeight(_ fitHeightItem: NSMenuItem) {
        fitHeightItem.menu?.deselectAllItems()
        fitHeightItem.state = .on
        
        scrollView.documentFitDimension = .height
        scrollView.magnifyDocumentViewToFit(animated: true)
        updateMagnificationTitle()
    }
    
    /// The Fit Page menu button has been pressed. Set the scrollView's documentFitDimension to both.
    @objc private func selectFitPage(_ fitPageItem: NSMenuItem) {
        fitPageItem.menu?.deselectAllItems()
        
        fitPageItem.state = .on
        scrollView.documentFitDimension = .both
        scrollView.magnifyDocumentViewToFit(animated: true)
        updateMagnificationTitle()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension NSMenu {
    /// Sets the state of all items to .off
    func deselectAllItems() {
        for item in items {
            item.state = .off
        }
    }
}
