//
//  ChartPageController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/24/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSStoryboard.SceneIdentifier {
    static let chartViewingController = NSStoryboard.SceneIdentifier("ChartViewingController")
}

class ChartViewingController: NSViewController {
    
    @IBOutlet weak var scrollView: FittingScrollView!
    @IBOutlet weak var chartView: ChartView!
    
    var document: ChartDocument! {
        return self.view.window?.windowController?.document as? ChartDocument
    }
    
    var pageSource: ChartPageSource!
    
    /// The key the chart is displaying the song of the ChartDocument in. This could be different from the ChartDocument's key.
    var viewingKey: MusicalNote = .c
    
    /// Set in loadSong() which is called for the first time in viewDidLoad()
    var songMeta: SongMeta! {
        didSet {
            guard pageSource != nil else { return }
            pageSource.songMeta = songMeta
        }
    }
    
    private func performOnEachPage(_ action: (ChartPageView)->()) {
        for page in chartView.pages {
            let chartPage = page as! ChartPageView
            action(chartPage)
        }
    }
    
    private var sectionViewCache: CachedMappingArray<Section, SectionView>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.scrollsDynamically = false
        scrollView.fittingDelegate = self
        chartView.autoresizingMask = .none
        chartView.itemSource = self
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        loadSong()
    }
    
    @objc func viewingKeyDidChange(_ keyPopupButton: NSPopUpButton) {
        guard let title = keyPopupButton.selectedItem?.title else { return }
        guard let newKey = MusicalNote(textSymbol: title) else { return }
        viewingKey = newKey
        
        loadSong()
    }
    
    /// Gets the most recent version of the song from the document.
    /// Transposes to the viewing key.
    /// Reloads the chart.
    func loadSong() {
        let transposedSong = document.song.transposed(to: viewingKey)
        // Reset the cache to be empty with the current song sections stored raw.
        sectionViewCache = CachedMappingArray<Section, SectionView>(transposedSong.sequence) { section in
            return SectionView(representedSection: section)
        }
        songMeta = transposedSong.meta
        pageSource = ChartPageSource(songMeta: songMeta)
        chartView.pageSource = pageSource
        chartView.reloadItems()
    }
}

extension ChartViewingController: SongObserver {
    
    func song<Value>(_ song: Song, didChangeMetaField field: KeyPath<SongMeta, Value>, newValue: Value) {
        songMeta = song.meta
        
        switch field {
        case \SongMeta.key:
            loadSong()
            // NOTE: This does not change the viewing key. This allows changes to the key that is used to interpret the chords.
        
        case \SongMeta.title:
            performOnEachPage { (page) in
                page.header.titleLabel.setStringValueHidingIfNil(newValue as? String)
            }
            
        case \SongMeta.authors:
            performOnEachPage { (page) in
                page.header.authorsLabel.setStringValueHidingIfNil(newValue as? String)
            }
            
        case \SongMeta.tempo:
            let tempo = newValue as? Double
            let string = (tempo != nil) ? tempoString(for: tempo!) : nil
            performOnEachPage { (page) in
                page.header.tempoLabel.setStringValueHidingIfNil(string)
            }
            
        case \SongMeta.measure:
            performOnEachPage { (page) in
                page.header.measureLabel.setStringValueHidingIfNil(newValue as? String)
            }
            
        case \SongMeta.copyright:
            performOnEachPage { (page) in
                page.footer.copyrightLabel.setStringValueHidingIfNil(newValue as? String)
            }
            chartView.reloadItems() // Copyright changes the area avaible to layout in.
            
        default:
            assertionFailure("Unexpected KeyPath")
        }
    }
    
    func song(_ song: Song, didRemoveSectionAtIndex index: Int) {
        sectionViewCache.remove(at: index)
        chartView.reloadItems()
    }
    
    func song(_ song: Song, didReplaceSectionAtIndex index: Int, with section: Section) {
        sectionViewCache.set(at: index, to: section.transposed(from: song.meta.key, to: viewingKey))
        chartView.reloadItems()
    }
    
    func song(_ song: Song, didMove section: Section, fromIndex: Int, toIndex: Int) {
        sectionViewCache.move(from: fromIndex, to: toIndex)
        chartView.reloadItems()
    }
    
    func song(_ song: Song, didInsert section: Section, atIndex index: Int) {
        sectionViewCache.insert(section.transposed(from: song.meta.key, to: viewingKey), at: index)
        chartView.reloadItems()
    }
}

extension ChartViewingController: ItemSource {
    
    func itemView(for index: Int) -> ItemView? {
        return sectionViewCache[index]
    }
    
    func numberOfItemViews() -> Int {
        return sectionViewCache.count
    }
}

extension ChartViewingController: FittingDelegate {
    
    func scrollViewSizeToFit(_ scrollView: FittingScrollView) -> NSSize? {
        return NSSize(width: ChartPageView.width, height: ChartPageView.height)
    }
}

private let bpmFormatter: NumberFormatter = {
    let nf = NumberFormatter()
    nf.positiveSuffix = " BPM"
    return nf
}()

private func tempoString(for tempo: Double) -> String {
    return bpmFormatter.string(from: NSNumber(value: tempo))!
}

extension ChartPageView {
    
    /// A helper method to set all the fields using a SongMeta.
    public func setFields(using songMeta: SongMeta) {
        header.titleLabel.setStringValueHidingIfNil(songMeta.title)
        header.authorsLabel.setStringValueHidingIfNil(songMeta.authors)
        header.keyLabel.setStringValueHidingIfNil(songMeta.key.textSymbol)
        let tempo = (songMeta.tempo != nil) ? tempoString(for: songMeta.tempo!) : nil
        header.tempoLabel.setStringValueHidingIfNil(tempo)
        header.measureLabel.setStringValueHidingIfNil(songMeta.measure)
        footer.copyrightLabel.setStringValueHidingIfNil(songMeta.copyright)
    }
}

