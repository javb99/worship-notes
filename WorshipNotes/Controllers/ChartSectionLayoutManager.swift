//
//  ChartSectionLayoutManager.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class _SectionContainer {
    
    var layoutSize: NSSize
    
    unowned var layoutManager: ChartSectionLayoutManager
    
    // Nil until added to a LayoutManager.
    var index: Int? = nil
    
    // Nil until this container has begun being laid out. A non-nil value does not guarantee the container is completely laid out. Check isCompletelyLaidOut to know if this is the full range.
    var containedSectionsRange: NSRange? = nil
    
    // Nil until this container has begun being laid out. A non-nil value does not guarantee the container is completely laid out. Check isCompletelyLaidOut to know if this is as full as the container will become.
    var filledHeight: CGFloat? = nil
    
    // True if the LayoutManager has finished laying out this container.
    var isCompletelyLaidOut: Bool = false
    
    init(size: NSSize, layoutManager: ChartSectionLayoutManager) {
        layoutSize = size
        self.layoutManager = layoutManager
    }
    
    /// Called by the LayoutManager. In the implementation ask for the SectionView.
    func finishedLayout() {
        
    }
    
    func laidOut(sectionAt index: Int) {
        
    }
    
//    func invalidatedLayout(startingAt index: Int) {
//
//    }
}

protocol ChartSectionLayoutManagerDelegate: AnyObject {
    func layoutManagerDidFinishLayout(for sectionContainer: _SectionContainer)
    func layoutManagerDidFinishLayout()
}

enum ChartSectionLayoutError: Error {
    /// This is thrown when the section doesn't fit in an empty container.
    case oversizedSection
}

class ChartSectionLayoutManager: NSObject {
    
    private struct SectionLayoutInfo {
        var section: Section
        var index: Int
        
        init(section: Section, index: Int) {
            self.section = section
            self.index = index
        }
        
        /// The sectionView for this section is cached here.
        var sectionView: SectionView?
        
        /// Once laid out this is the container that holds this section.
        var container: _SectionContainer?
        
        /// The rectangle, in container space, this section is occupying.
        var rectInContainer: NSRect?
    
        
        var isLaidOut: Bool {
            return sectionView != nil && container != nil && rectInContainer != nil
        }
    }
    
    /// The vertical spacing between sections. Not added at the top or bottom.
    static let interSectionVerticalPadding: CGFloat = 5
    
    /// A SectionLayoutInfo is created for every section that is added even before it is laid out. There will always be a SectionLayoutInfo for a held section.
    private var sections: [SectionLayoutInfo] = []
    
    public func setSections(_ newSections: [Section]) {
        var sectionInfos = [SectionLayoutInfo]()
        for i in 0..<newSections.count {
            sectionInfos.append(SectionLayoutInfo(section: newSections[i], index: i))
        }
        sections = sectionInfos
        // TODO: Reset state.
    }
    
    /// A ContainerInfo is created for every container that is added even before it is laid out. There will always be a ContainerInfo for a managedContainer.
    private(set) var containers: [_SectionContainer] = []
    
    public func add(_ container: _SectionContainer) {
        let index = containers.endIndex
        containers.append(container)
        container.index = index
    }
    
    public func remove(_ container: _SectionContainer) {
        fatalError("Unimplemented")
    }
    
    public var delegate: ChartSectionLayoutManagerDelegate?
    
    /// Call this to make sure the layout is cached for the given container.
//    public func ensureContainerIsLaidOut(_ container: _SectionContainer) {
//        // TODO: Optimize to limit calls.
//        layout(through: container)
//    }
    
    /// Call this to make sure the layout is cached for the given range of sections.
    public func ensureLayout(throughSectionAt sectionIndex: Int) {
        // TODO: Optimize to limit calls.
        layout(throughSectionIndex: sectionIndex)
    }
    
    /// Invalidate the layout information for whole range of sections. This could also re layout all the following sections.
    func invalidateLayout(forSectionRange: NSRange) {
        fatalError("Unimplemented")
        // Don't forget to change the unlaidSectionIndex related variables.
    }
    
    public func sectionView(forSectionAt index: Int) -> SectionView? {
        return sections[index].sectionView
    }
    
    /// Return the section view for the specified index or create one if it's nil.
    private func generateSectionViewIfNeeded(forSectionAt index: Int) -> SectionView {
        if let sectionView = sectionView(forSectionAt: index) {
            return sectionView
        } else {
            let section = sections[index].section
            // TODO: Account for container width.
            let sectionView = SectionView(representedSection: section)
            sections[index].sectionView = sectionView
            return sectionView
        }
    }
    
//    public func sectionRange(for container: _SectionContainer) -> NSRange? {
//        return
//    }
    
    public func frame(forSectionAt index: Int) -> NSRect? {
        return sections[index].rectInContainer
    }
    
    /// The index of the first section that has not been laid out yet.
    var firstUnlaidSectionIndex: Int?
    
    /// The first container that has room for sections.
    var firstUnfilledContainerIndex: Int?
    
    /// Lays out the sections for all the containers before and up to the given section that have not already done so.
    private func layout(throughSectionIndex lastSectionToLayout: Int) {
    //private func layout(through container: _SectionContainer) {
        //guard containers.contains(where: { container.index == $0.index }) else { return }
        
        guard containers.count > 0 else { return } // TODO: Think about what might need to happen.
        
        //var finishedGivenContainer = false
        var sectionIndex: Int = firstUnlaidSectionIndex ?? 0
        var currentContainerIndex: Int = firstUnfilledContainerIndex ?? 0
        
        while sectionIndex <= lastSectionToLayout /*|| finishedGivenContainer */{
            
            do {
                if try !does(sectionAt: sectionIndex, fitIn: containers[currentContainerIndex]) {
                    // The section does not fit in the current container.
                    // Move to the next container and inform the delegate of the finished container.
                    
                    delegate?.layoutManagerDidFinishLayout(for: containers[currentContainerIndex])
                    containers[currentContainerIndex].finishedLayout()
//                    if containers[currentContainerIndex] === container {
//                        // We have finished the given container. Stop looping.
//                        finishedGivenContainer = true
//                    }
                    currentContainerIndex += 1
                    continue
                }
            } catch ChartSectionLayoutError.oversizedSection {
                print("Oversized Section!")
                break
            } catch {}
            
            layout(sectionAt: sectionIndex, in: containers[currentContainerIndex])
            containers[currentContainerIndex].laidOut(sectionAt: sectionIndex)
            
            sectionIndex += 1
        }
        
        if sectionIndex == sections.count {
            delegate?.layoutManagerDidFinishLayout()
            // TODO: Mark containers as complete.
        } else {
            firstUnlaidSectionIndex = sectionIndex
            // TODO: Calculate first unlaid container.
        }
    }
    
    private func does(sectionAt index: Int, fitIn container: _SectionContainer) throws -> Bool {
        //let section = sections[index].section
        let sectionView = generateSectionViewIfNeeded(forSectionAt: index)
        guard !container.isCompletelyLaidOut else { return false }
        
        let maxArea = container.layoutSize
        let filledHeight = container.filledHeight ?? 0
        let isEmpty = filledHeight == 0
        
        let usableHeight = maxArea.height - filledHeight
        let interSectionGap = !isEmpty ? ChartSectionLayoutManager.interSectionVerticalPadding : 0
        
        
        if (interSectionGap + sectionView.frame.height) < usableHeight  {
            // The section will fit.
            return true
        } else if isEmpty {
            // The container is empty but the section still doesn't fit.
            throw ChartSectionLayoutError.oversizedSection
        } else {
            // The section won't fit.
            return false
        }
    }
    
    private func layout(sectionAt index: Int, in container: _SectionContainer) {
        //print("Laying section #\(index) in container #\(container.index!).")
        let sectionView = generateSectionViewIfNeeded(forSectionAt: index)
        
        let maxArea = container.layoutSize
        // Nil results in a zero size rect at the top left corner.
        let filledHeight = container.filledHeight ?? 0
        let isEmpty = filledHeight == 0
        let topEdgeOfUsableArea = maxArea.height - filledHeight
        let interSectionGap = !isEmpty ? ChartSectionLayoutManager.interSectionVerticalPadding : 0
        let y = topEdgeOfUsableArea - interSectionGap - sectionView.frame.height
        
        sections[index].container = container
        let frame = NSRect(x: 0, y: y, width: sectionView.frame.width, height: sectionView.frame.height)
        sections[index].rectInContainer = frame
        
        container.filledHeight = filledHeight + interSectionGap + frame.height
        // Add 1 to the length of containedSectionsRange
        container.containedSectionsRange = NSRange(location: container.containedSectionsRange?.location ?? index, length: (container.containedSectionsRange?.length ?? 0) + 1)
    }
}
