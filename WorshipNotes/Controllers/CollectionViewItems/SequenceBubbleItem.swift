//
//  SequenceBubbleItem.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 1/29/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSNib.Name {
    static let sequenceBubbleItem = NSNib.Name("SequenceBubbleItem")
}

extension NSUserInterfaceItemIdentifier {
    static let sequenceBubbleItemIdentifier = NSUserInterfaceItemIdentifier(rawValue: "SequenceBubbleItem")
}

enum ContextMenuItem: Int {
    case duplicate = 1
    case incrementReps = 2
    case decrementReps = 3
    case rename = 4
    case remove = 5
}

protocol SequenceBubbleItemContextMenuDelegate {
    func menuItemPressed(_ item: ContextMenuItem, in bubble: SequenceBubbleItem)
    func validateMenuItem(_ item: ContextMenuItem, in bubble: SequenceBubbleItem) -> Bool
}

extension SequenceBubbleItemContextMenuDelegate {
    func validateMenuItem(_ item: ContextMenuItem, in bubble: SequenceBubbleItem) -> Bool {
        return true
    }
}

class SequenceBubbleItem: NSCollectionViewItem {
    
    var bubble: BubbleView!
    
    var contextMenuDelegate: SequenceBubbleItemContextMenuDelegate?
    
    var reps: Int = 0 {
        didSet {
            bubble.reps = reps
        }
    }
    
    var abriviation: String = "" {
        didSet {
            bubble.text = abriviation
        }
    }
    
    var fillColor: NSColor = .white {
        didSet {
            bubble.fillColor = fillColor
        }
    }
    
    override var isSelected: Bool {
        didSet {
            updateBorder()
        }
    }
    
    override var highlightState: NSCollectionViewItem.HighlightState {
        didSet {
            updateBorder()
        }
    }
    
    override func loadView() {
        bubble = BubbleView()
        view = bubble
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBorder()
    }
    
    func updateBorder() {
        switch highlightState {
        case .forSelection:
            bubble.stateBorderColor = NSColor.orange
        case .none:
            bubble.stateBorderColor = isSelected ? NSColor.blue : nil
        case .asDropTarget, .forDeselection:
            break
        }
    }
    
    @IBAction func contextMenuItemPressed(_ sender: NSMenuItem) {
        guard let item = ContextMenuItem(rawValue: sender.tag) else { return }
        contextMenuDelegate?.menuItemPressed(item, in: self)
    }
    
    override func rightMouseDown(with event: NSEvent) {
        let location = view.convert(event.locationInWindow, from: nil)
        showContextMenuIfPointIsOnMainCircle(location)
        super.rightMouseDown(with: event)
    }
    
    override func mouseDown(with event: NSEvent) {
        if event.modifierFlags.contains(.command) {
            let location = view.convert(event.locationInWindow, from: nil)
            showContextMenuIfPointIsOnMainCircle(location)
        }
        super.mouseDown(with: event)
    }
    
    func showContextMenuIfPointIsOnMainCircle(_ point: NSPoint) {
        let center = bubble.bubbleLayer!.mainCircle.frame.center
        let squaredDistance = (point.x - center.x)*(point.x - center.x) + (point.y - center.y)*(point.y - center.y)
        if squaredDistance <= SectionCircleLayer.radius*SectionCircleLayer.radius {
            showContextMenu(at: point)
        }
    }
    
    func showContextMenu(at point: NSPoint) {
        let menu = NSMenu()
        let duplicate = NSMenuItem(title: "Duplicate", action: #selector(contextMenuItemPressed(_:)), keyEquivalent: "")
        duplicate.target = self
        duplicate.tag = 1
        let incrementReps = NSMenuItem(title: "Times +1", action: #selector(contextMenuItemPressed(_:)), keyEquivalent: "")
        incrementReps.target = self
        incrementReps.tag = 2
        let decrementReps = NSMenuItem(title: "Times -1", action: #selector(contextMenuItemPressed(_:)), keyEquivalent: "")
        decrementReps.target = self
        decrementReps.tag = 3
        let remove = NSMenuItem(title: "Remove", action: #selector(contextMenuItemPressed(_:)), keyEquivalent: "")
        remove.target = self
        remove.tag = 5
        menu.insertItem(duplicate, at: 0)
        menu.insertItem(incrementReps, at: 1)
        menu.insertItem(decrementReps, at: 2)
        menu.insertItem(remove, at: 3)
        
        menu.popUp(positioning: nil, at: point, in: self.view)
    }
    
    override func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
        guard let item = ContextMenuItem(rawValue: menuItem.tag) else { return false }
        return contextMenuDelegate?.validateMenuItem(item, in: self) ?? false
    }
}
