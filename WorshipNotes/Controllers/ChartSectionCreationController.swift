//
//  ChartSectionCreationController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/13/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSStoryboard.SceneIdentifier {
    static let chartSectionCreationController = NSStoryboard.SceneIdentifier("ChartSectionCreationController")
}

class ChartSectionCreationController: NSViewController {
    
    @IBOutlet weak var sequenceScrollView: NSScrollView!
    
    @IBOutlet weak var sequenceCollectionView: NSCollectionView!
    
    @IBOutlet weak var sectionNotesField: NSTextField!
    
    @IBOutlet var sectionContentTextView: NSTextView!
    
    @IBOutlet weak var sectionTitleBox: NSComboBox!
    
    @IBOutlet weak var sectionAbriviationField: NSTextField!
    
    var document: ChartDocument!
    
    /// This is not meant to be used for anything the user sees.
    static let hiddenNumberFormatter = NumberFormatter()
    
    /// Key is the section title. Value is the abriviation.
    var sectionTitleAbrivations: [String: String] = [:] {
        didSet {
            for title in sectionTitleAbrivations.keys {
                if !oldValue.keys.contains(title) {
                    sectionTitleBox.addItem(withObjectValue: title)
                }
            }
        }
    }
    
    /// The index of the section that is being editted. Nil is considered no selection. Use select(sectionAt:); don't change this directly.
    var selectedIndex: Int? = nil
    
    /// The section that is being shown and is available for editing. Nil is considered no selection. Use select(sectionAt:); don't change this directly.
    var selectedSection: Section? = nil
    
    /// The index that is currently being dragged.
    var indexBeingDragged: IndexPath? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //document.songObservers.append(self) // TODO: only once
        
        sectionTitleAbrivations = ["Intro": "I", "Verse 1": "V1", "Chorus": "C", "Bridge": "B", "Instrumental": "Is", "Ending": "E"]
        
        sectionTitleBox.delegate = self
        
        configureSequenceCollectionView()
        
        wireUpTextViews()
    }
    
    /// Set the delegate and tag of the TextFields.
    func wireUpTextViews() {
        
        sectionAbriviationField.delegate = self
        sectionAbriviationField.tag = 1
        
        sectionNotesField.delegate = self
        sectionNotesField.tag = 2
        
        sectionContentTextView.delegate = self
        sectionContentTextView.isRichText = false
    }
    
    /// Sets the properties on the sequence view that are expected.
    func configureSequenceCollectionView() {
        sequenceCollectionView.delegate = self
        sequenceCollectionView.dataSource = self
        
        sequenceCollectionView.wantsLayer = true
        
        sequenceCollectionView.register(SequenceBubbleItem.self, forItemWithIdentifier: .sequenceBubbleItemIdentifier)
        (sequenceCollectionView.collectionViewLayout as! NSCollectionViewFlowLayout).itemSize = NSSize(width: 37, height: 37)
        
        // Drag and Drop
        sequenceCollectionView.setDraggingSourceOperationMask(.move, forLocal: true)
        sequenceCollectionView.setDraggingSourceOperationMask(.every, forLocal: false)
        sequenceCollectionView.registerForDraggedTypes([.string])
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        sequenceCollectionView.reloadData()
        print("Content Size: \(sequenceCollectionView.intrinsicContentSize)")
    }
    
    /// Officially makes the change with the ChartDocument. This will cause a notification to be sent to any interested parties, including the viewing controller.
    func pushSectionUpdate() {
        guard selectedIndex != nil, let section = selectedSection else { return }
        document.replaceSection(atIndex: selectedIndex!, with: section)
    }
    
    func select(sectionAt index: Int?) {
        //guard index == selectedIndex else { return }
        // Only run if the index actually changed.
        
        if let index = index {
            // Store the section at the index and load the information into the text fields.
            
            let selection = min(index, document.songSequence.count-1)
            
            sequenceCollectionView.selectionIndexPaths = [IndexPath(item: selection, section: 0)]
            
            let section = document.song.sequence[selection]
            sectionTitleBox.stringValue = section.title
            sectionAbriviationField.stringValue = section.abriviation
            sectionNotesField.stringValue = section.notes
            sectionContentTextView.string = section.rawText
            
            selectedSection = section
            selectedIndex = selection // After to stop above call from reloading the index
            
        } else {
            // Clear the selection and empty the text fields.
            
            selectedIndex = nil
            selectedSection = nil
            sequenceCollectionView.selectionIndexPaths = []
            
            sectionTitleBox.stringValue = "No Section"
            sectionAbriviationField.stringValue = ""
            sectionNotesField.stringValue = ""
            sectionContentTextView.string = ""
        }
        
        //print("Selection is now \(selectedIndex ?? -1) according to CreationController")
        //print("Selection is now \(sequenceCollectionView.selectionIndexPaths.first?.item ?? -1) according to SequenceCollectionView")
    }
    
    /// Adds a new section either after the selected section or at the end of the sequence and selects it.
    @IBAction func addSection(_ sender: NSButton) {
        sequenceScrollView.needsDisplay = true
        
        let section = Section(abriviation: "C", title: "Chorus")
        if let selection = selectedIndex {
            document.insert(section, atIndex: selection)
        } else {
            document.append(section)
        }
    }
    
    @IBAction func remove(_ sender: Any) {
        guard let curSectionIndex = selectedIndex else  { return }
        
        // Remove the selected item from the data model and animate the change.
        document.removeSection(atIndex: curSectionIndex)
        
        if document.song.sequence.count == 0 {
            // There are no items to select. Clear the selection
            select(sectionAt: nil)
        } else if curSectionIndex-1 < 0 {
            // The first item was deleted. Select the new first item.
            select(sectionAt: 0)
        } else {
            // Select the item before the deleted item.
            select(sectionAt: curSectionIndex-1)
        }
    }
    
    @objc func incrementReps(_ menuItem: NSMenuItem) {
        guard let title = menuItem.menu?.title, let index = ChartSectionCreationController.hiddenNumberFormatter.number(from: title)?.intValue else { return }
        
        var section = document.songSequence[index]
        section.repetitions += 1
        document.replaceSection(atIndex: index, with: section)
    }
}

extension ChartSectionCreationController: NSControlTextEditingDelegate {
    
    override func controlTextDidChange(_ obj: Notification) {
        guard let fieldEditor = obj.object as? NSTextField else { return }
        
        switch fieldEditor.tag {
        case 1:
            selectedSection?.abriviation = sectionAbriviationField.stringValue
            pushSectionUpdate()
        case 2:
            selectedSection?.notes = sectionNotesField.stringValue
            pushSectionUpdate()
        default:
            fatalError("Unexpected Tag")
        }
    }
}

extension ChartSectionCreationController: NSTextViewDelegate {
    
    func textDidChange(_ notification: Notification) {
        let textView = notification.object as! NSTextView
        if textView == sectionContentTextView {
            selectedSection?.rawText = sectionContentTextView.string
            pushSectionUpdate()
        }
    }
}

extension ChartSectionCreationController: NSCollectionViewDataSource {
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return document?.song.sequence.count ?? 0
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let item = collectionView.makeItem(withIdentifier: .sequenceBubbleItemIdentifier, for: indexPath) as! SequenceBubbleItem
        let section = document.songSequence[indexPath.item]
        item.abriviation = section.abriviation
        item.reps = section.repetitions
        item.fillColor = .randomColor()
        item.isSelected = collectionView.selectionIndexPaths.contains(indexPath)
        item.contextMenuDelegate = self
        
        print("Queried for item at \(indexPath.item)")
        
        return item
    }
}

extension ChartSectionCreationController: SequenceBubbleItemContextMenuDelegate {
    
    func menuItemPressed(_ item: ContextMenuItem, in bubble: SequenceBubbleItem) {
        guard let indexPath = sequenceCollectionView.indexPath(for: bubble) else { return }
        let index = indexPath.item
        
        switch item {
        case .duplicate:
            document.insert(document.songSequence[index], atIndex: index)
            
        case .incrementReps:
            var section = document.songSequence[index]
            section.repetitions += 1
            document.replaceSection(atIndex: index, with: section)
            
        case .decrementReps:
            var section = document.songSequence[index]
            section.repetitions -= 1
            document.replaceSection(atIndex: index, with: section)
            
        case .remove:
            document.removeSection(atIndex: index)
            
        case .rename:
            break
        }
    }
    
    func validateMenuItem(_ item: ContextMenuItem, in bubble: SequenceBubbleItem) -> Bool {
        guard let indexPath = sequenceCollectionView.indexPath(for: bubble) else { return false }
        switch item {
        case .decrementReps:
            let reps = document.songSequence[indexPath.item].repetitions
            return reps > 1
        default:
            return true
        }
    }
}

extension ChartSectionCreationController: NSCollectionViewDelegate {
    
    func collectionView(_ collectionView: NSCollectionView, didEndDisplaying item: NSCollectionViewItem, forRepresentedObjectAt indexPath: IndexPath) {
        print("Stopped displaying item at: \(indexPath.item)")
    }
    
    func collectionView(_ collectionView: NSCollectionView, willDisplay item: NSCollectionViewItem, forRepresentedObjectAt indexPath: IndexPath) {
        print("Will display item at: \(indexPath.item)")
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        select(sectionAt: indexPaths.first?.item)
    }
    
    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        select(sectionAt: nil)
    }
    
    func collectionView(_ collectionView: NSCollectionView, pasteboardWriterForItemAt index: Int) -> NSPasteboardWriting? {
        return "Song Section" as NSString
    }
    
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItemsAt indexPaths: Set<IndexPath>) {
        indexBeingDragged = indexPaths.first
    }
    
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, dragOperation operation: NSDragOperation) {
        indexBeingDragged = nil
    }
    
    func collectionView(_ collectionView: NSCollectionView, validateDrop draggingInfo: NSDraggingInfo, proposedIndexPath proposedDropIndexPath: AutoreleasingUnsafeMutablePointer<NSIndexPath>, dropOperation proposedDropOperation: UnsafeMutablePointer<NSCollectionView.DropOperation>) -> NSDragOperation {
        return .move
    }
    
    func collectionView(_ collectionView: NSCollectionView, acceptDrop draggingInfo: NSDraggingInfo, indexPath: IndexPath, dropOperation: NSCollectionView.DropOperation) -> Bool {
        
        guard let fromIndexPath = indexBeingDragged else { return false }
        let section = document.song.sequence[fromIndexPath.item]
        
        let fromIndex = fromIndexPath.item
        let toIndex = indexPath.item
        
        document.move(section, fromIndex: fromIndex, toIndex: toIndex)
        
        return true
    }
}

extension ChartSectionCreationController: NSComboBoxDelegate {
    
    @IBAction func sectionTitleChanged(_ sender: NSComboBox) {
        let title = sectionTitleBox.stringValue
        selectedSection?.title = title
        if let abr = sectionTitleAbrivations[title] {
            sectionAbriviationField.stringValue = abr
            selectedSection?.abriviation = abr
        }
        pushSectionUpdate()
    }
}

extension ChartSectionCreationController: SongObserver {
    
    func song(_ song: Song, didRemoveSectionAtIndex index: Int) {
        sequenceCollectionView.animator().deleteItems(at: [IndexPath(item: index, section: 0)])
        //sequenceCollectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
    }
    
    func song(_ song: Song, didReplaceSectionAtIndex index: Int, with section: Section) {
        sequenceCollectionView.animator().reloadItems(at: [IndexPath(item: index, section: 0)])
        //sequenceCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
    }
    
    func song(_ song: Song, didMove section: Section, fromIndex: Int, toIndex: Int) {
        sequenceCollectionView.animator().moveItem(at: IndexPath(item: fromIndex, section: 0), to: IndexPath(item: toIndex, section: 0))
        //sequenceCollectionView.moveItem(at: IndexPath(item: fromIndex, section: 0), to: IndexPath(item: toIndex, section: 0))
    }
    
    func song(_ song: Song, didInsert section: Section, atIndex index: Int) {
        let indexPath = IndexPath(item: min(index, song.sequence.endIndex-1), section: 0)
        sequenceCollectionView.animator().insertItems(at: [indexPath])
        //sequenceCollectionView.insertItems(at: [indexPath])
        select(sectionAt: index)
    }
}
