//
//  HighLevelTextController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 1/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class HighLevelTextController: NSViewController {
    
    @IBOutlet var textView: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        textView.textContainer?.exclusionPaths = pathsOutliningOvalHole(in: textView.bounds)
        textView.layoutManager?.delegate = self
    }
    
    func pathsOutliningOvalHole(in rect: NSRect) -> [NSBezierPath] {
        let b = rect
        let topLeft = NSPoint(x: b.minX, y: b.maxY)
        let midLeft = NSPoint(x: b.minX, y: b.midY)
        let bottomLeft = NSPoint(x: b.minX, y: b.minY)
        
        let topRight = NSPoint(x: b.maxX, y: b.maxY)
        let midRight = NSPoint(x: b.maxX, y: b.midY)
        let bottomRight = NSPoint(x: b.maxX, y: b.minY)
        
        let topMiddle = NSPoint(x: b.midX, y: b.maxY)
        let bottomMiddle = NSPoint(x: b.midX, y: b.minY)
        
        let topLeftCurve = NSBezierPath()
        topLeftCurve.move(to: midLeft)
        topLeftCurve.curve(to: topMiddle, controlPoint1: topLeft, controlPoint2: topLeft)
        topLeftCurve.line(to: topLeft)
        topLeftCurve.line(to: midLeft)
        topLeftCurve.close()
        
        let topRightCurve = NSBezierPath()
        topRightCurve.move(to: midRight)
        topRightCurve.curve(to: topMiddle, controlPoint1: topRight, controlPoint2: topRight)
        topRightCurve.line(to: topRight)
        topRightCurve.line(to: midRight)
        topRightCurve.close()
        
        let bottomLeftCurve = NSBezierPath()
        bottomLeftCurve.move(to: midLeft)
        bottomLeftCurve.curve(to: bottomMiddle, controlPoint1: bottomLeft, controlPoint2: bottomLeft)
        bottomLeftCurve.line(to: bottomLeft)
        bottomLeftCurve.line(to: midLeft)
        bottomLeftCurve.close()
        
        
        let bottomRightCurve = NSBezierPath()
        bottomRightCurve.move(to: midRight)
        bottomRightCurve.curve(to: bottomMiddle, controlPoint1: bottomRight, controlPoint2: bottomRight)
        bottomRightCurve.line(to: bottomRight)
        bottomRightCurve.line(to: midRight)
        bottomRightCurve.close()
        
        return [topLeftCurve, topRightCurve, bottomLeftCurve, bottomRightCurve]
    }
    
    override func viewDidLayout() {
        textView.textContainer?.exclusionPaths = pathsOutliningOvalHole(in: textView.bounds)
    }
    
}

extension HighLevelTextController: NSLayoutManagerDelegate {
    
    //let lineHeightMultiple: CGFloat = 1.6
    
    
    public func layoutManager(
        _ layoutManager: NSLayoutManager,
        shouldSetLineFragmentRect lineFragmentRect: UnsafeMutablePointer<NSRect>,
        lineFragmentUsedRect: UnsafeMutablePointer<NSRect>,
        baselineOffset: UnsafeMutablePointer<CGFloat>,
        in textContainer: NSTextContainer,
        forGlyphRange glyphRange: NSRange) -> Bool {
        
        let font: NSFont = NSFont.systemFont(ofSize: NSFont.systemFontSize)
        
        let fontLineHeight = layoutManager.defaultLineHeight(for: font)
        let lineHeight = fontLineHeight * 2
        let baselineNudge = (lineHeight - fontLineHeight)
            // The following factor is a result of experimentation:
            * 0.6
        
        
        var rect = lineFragmentRect.pointee
        //rect.size.height = lineHeight
        lineFragmentRect.pointee = rect.offsetBy(dx: 0, dy: fontLineHeight)
        
        
        var usedRect = lineFragmentUsedRect.pointee
        //usedRect.size.height = max(lineHeight, usedRect.size.height) // keep emoji sizes
        
        //lineFragmentRect.pointee = rect
        lineFragmentUsedRect.pointee = usedRect.offsetBy(dx: 0, dy: fontLineHeight)
        //baselineOffset.pointee = baselineOffset.pointee + fontLineHeight
        
        return true
    }
}
