//
//  ChartInfoCreationController.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/13/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSStoryboard.SceneIdentifier {
    static let chartInfoCreationController = NSStoryboard.SceneIdentifier("ChartInfoCreationController")
}

class ChartInfoCreationController: NSViewController {
    
    @IBOutlet weak var titleField: NSTextField!
    
    @IBOutlet weak var authorsField: NSTextField!
    
    @IBOutlet weak var keyField: NSTextField!
    
    @IBOutlet weak var tempoField: NSTextField!
    
    @IBOutlet weak var meterField: NSTextField!
    
    @IBOutlet var copyrightTextView: NSTextView!
    
    var document: ChartDocument!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //document.songObservers.append(self) // TODO: only once

        wireUpTextViews()
    }
    
    /// Set the delegate and tag of the TextFields.
    func wireUpTextViews() {
        
        titleField.delegate = self
        titleField.tag = 1
        
        authorsField.delegate = self
        authorsField.tag = 2
        
        keyField.delegate = self
        keyField.tag = 3
        
        tempoField.delegate = self
        tempoField.tag = 4
        
        meterField.delegate = self
        meterField.tag = 5
        
        copyrightTextView.delegate = self
        copyrightTextView.isRichText = false
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        setFields(with: document.song.meta)
    }

    /// Update the fields based on the SongMeta.
    func setFields(with songMeta: SongMeta) {
        titleField.stringValue = songMeta.title
        authorsField.stringValue = songMeta.authors ?? ""
        keyField.stringValue = songMeta.key.textSymbol
        if let tempo = songMeta.tempo { tempoField.doubleValue = tempo }
        meterField.stringValue = songMeta.measure ?? ""
        copyrightTextView.string = songMeta.copyright ?? ""
    }
}

extension ChartInfoCreationController: NSTextFieldDelegate {
    
    override func controlTextDidChange(_ obj: Notification) {
        guard let fieldEditor = obj.object as? NSTextField else { return }
        
        switch fieldEditor.tag {
        case 1:
            document.songTitle = titleField.stringValue
        case 2:
            document.songAuthors = authorsField.stringValue
        case 3:
            document.songKey = MusicalNote(textSymbol: keyField.stringValue) ?? .c
        case 4:
            document.songTempo = tempoField.doubleValue
        case 5:
            document.songMeasure = meterField.stringValue
        default:
            fatalError("Unexpected Tag")
        }
    }
}

extension ChartInfoCreationController: NSTextViewDelegate {
    
    func textDidChange(_ notification: Notification) {
        let textView = notification.object as! NSTextView
        if textView == copyrightTextView {
            document.songCopyright = textView.string
        }
    }
}

extension ChartInfoCreationController: SongObserver {
    
    func song<Value>(_ song: Song, didChangeMetaField field: KeyPath<SongMeta, Value>, newValue: Value) {
        setFields(with: song.meta)
    }
}
