//
//  MagnifiableTextLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/13/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class MagnifiableTextLayer: CATextLayer {
    
    let maxMagnificationFactor: CGFloat
    
    init(maxMagnificationFactor: CGFloat = 1, screenContentScale: CGFloat = 1) {
        self.maxMagnificationFactor = maxMagnificationFactor
        super.init()
        contentsScale = maxMagnificationFactor * screenContentScale
    }
    
    override init(layer: Any) {
        guard let sourceLayer = layer as? MagnifiableTextLayer else {
            fatalError("Type of layer doesn't match expected MagnifiableTextLayer.")
        }
        maxMagnificationFactor = sourceLayer.maxMagnificationFactor
        super.init(layer: layer)
    }
    
    override var contentsScale: CGFloat {
        get {
            return super.contentsScale
        }
        set {
            super.contentsScale = newValue * maxMagnificationFactor
        }
    }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        
        ctx.setAllowsAntialiasing(true)
        ctx.setAllowsFontSmoothing(true)
        ctx.setAllowsFontSubpixelPositioning(true)
        ctx.setAllowsFontSubpixelQuantization(true)
        
        ctx.setFillColor(backgroundColor ?? .white);
        ctx.fill(bounds);
        
        ctx.setShouldAntialias(true)
        ctx.setShouldSmoothFonts(true)
        ctx.setShouldSubpixelPositionFonts(true)
        ctx.setShouldSubpixelQuantizeFonts(true)
        
        super.draw(in: ctx)
        
        ctx.restoreGState()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Unimplemented")
    }
}
