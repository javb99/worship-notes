//
//  SectionCircleLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/26/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

class SectionCircleLayer: CALayer {
    
    /// The radius of the main circle.
    static let radius: CGFloat = 14
    
    /// The font size to use in the main circle. Use a bold font.
    static let fontSize: CGFloat = 11
    
    /// The radius of the smaller repetitions circle.
    static let repRadius = SectionCircleLayer.radius/2
    
    /// The font size to use in the smaller repetitions circle.
    static let repFontSize: CGFloat = 9
    
    /// The distance from the bottom of the bounds to the bottom of the text. (Or baseline?)
    var textBaseline: CGFloat
    
    
    var outerBorder: CALayer
    
    var mainCircle: CircumscribedTextLayer
    
    var repsCircle: CircumscribedTextLayer
    
    var reps: Int {
        didSet {
            repsCircle.textLayer.string = "\(reps)"
            hideRepsIfNeeded()
        }
    }
    
    var text: String {
        didSet {
            mainCircle.textLayer.string = text
        }
    }
    
    var fillColor: NSColor = .white {
        didSet {
            mainCircle.backgroundColor = fillColor.cgColor
            mainCircle.textLayer.foregroundColor = fillColor.wantsLightFont ? .white : .black
        }
    }
    
    /// Nil denotes a hidden border. This is the border color to set for highlighting. The text colored border is always visible.
    var stateBorderColor: NSColor? = nil {
        didSet {
            refreshStateBorder()
        }
    }
    
    init(text: String, repetitions reps: Int) {
        self.text = text
        self.reps = reps
        
        let textColor = CGColor.black
        
        mainCircle = CircumscribedTextLayer(text: text, radius: SectionCircleLayer.radius, fontSize: SectionCircleLayer.fontSize, fill: fillColor.cgColor, stroke: textColor)
        let textLayer = mainCircle.textLayer
        textBaseline = textLayer.frame.minY
        
        repsCircle = CircumscribedTextLayer(text: "\(reps)", radius: SectionCircleLayer.repRadius, fontSize: SectionCircleLayer.repFontSize)
        
        let outerBorderRadius = SectionCircleLayer.radius + 2
        outerBorder = CALayer()
        outerBorder.bounds = CGRect(x: 0, y: 0, width: outerBorderRadius*2, height: outerBorderRadius*2)
        outerBorder.cornerRadius = outerBorderRadius
        outerBorder.backgroundColor = stateBorderColor?.cgColor
        
        super.init()
        
        anchorPoint = CGPoint.zero
        contentsScale = 2.0
        
        outerBorder.anchorPoint = CGPoint(x: 0, y: 0)
        outerBorder.position = CGPoint.zero
        addSublayer(outerBorder)
        
        mainCircle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        mainCircle.position = outerBorder.frame.center
        addSublayer(mainCircle)
        
        repsCircle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        repsCircle.position = CGPoint(x: mainCircle.frame.maxX, y: mainCircle.frame.maxY)
        addSublayer(repsCircle)
        
        bounds = CGRect(x: 0, y: 0, width: repsCircle.frame.maxX - outerBorder.frame.minX, height: repsCircle.frame.maxY - outerBorder.frame.minY)
        
        hideRepsIfNeeded()
        refreshStateBorder()
    }
    
    override init(layer: Any) {
        guard let sourceLayer = layer as? SectionCircleLayer else {
            fatalError("Type of layer doesn't match expected SectionCircleLayer.")
        }
        mainCircle = sourceLayer.mainCircle
        repsCircle = sourceLayer.repsCircle
        outerBorder = sourceLayer.outerBorder
        reps = sourceLayer.reps
        text = sourceLayer.text
        fillColor = sourceLayer.fillColor
        textBaseline = sourceLayer.textBaseline
        
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func hideRepsIfNeeded() {
        repsCircle.isHidden = reps <= 1
    }
    
    func refreshStateBorder() {
        if let stateBorderColor = stateBorderColor {
            outerBorder.backgroundColor = stateBorderColor.cgColor
            outerBorder.isHidden = false
        } else {
            outerBorder.isHidden = true
        }
    }
}

class CircumscribedTextLayer: CALayer {
    
    var textLayer: FittingTextLayer
    
    init(text: String, radius: CGFloat, fontSize: CGFloat, fill: CGColor = .white, stroke: CGColor = .black) {
        textLayer = FittingTextLayer(maxMagnificationFactor: 4.0)
        textLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        textLayer.foregroundColor = stroke
        textLayer.backgroundColor = .clear
        textLayer.string = text
        let font = NSFont.boldSystemFont(ofSize: fontSize)
        textLayer.nsfont = font
        textLayer.alignmentMode = convertToCATextLayerAlignmentMode(convertFromCATextLayerAlignmentMode(CATextLayerAlignmentMode.center))
        textLayer.contentsScale = 2.0
        textLayer.name = "Internal Text Layer"
        
        super.init()
        bounds = CGRect(x: 0, y: 0, width: radius*2, height: radius*2)
        anchorPoint = CGPoint.zero
        cornerRadius = radius
        backgroundColor = fill
        borderColor = stroke
        borderWidth = 1
        contentsScale = 2.0
        name = "Circumscribed Text Layer"
        
        textLayer.position = bounds.center
        
        addSublayer(textLayer)
    }
    
    override init(layer: Any) {
        guard let sourceLayer = layer as? CircumscribedTextLayer else {
            fatalError("Type of layer doesn't match expected CircumscribedTextLayer.")
        }
        textLayer = sourceLayer.textLayer
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToCATextLayerAlignmentMode(_ input: String) -> CATextLayerAlignmentMode {
	return CATextLayerAlignmentMode(rawValue: input)
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATextLayerAlignmentMode(_ input: CATextLayerAlignmentMode) -> String {
	return input.rawValue
}
