//
//  SectionLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/26/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

class SectionLayer: CALayer {
    
    /// Vertical spacing between sections.
    static let verticalSpacing: CGFloat = ChartPageView.margins.bottom
    
    /// Horizontal spacing between sections.
    static let horizontalSpacing: CGFloat = ChartPageView.margins.left
    
    /// The width of the section.
    static let width: CGFloat = (ChartPageView.usablePage.width - SectionLayer.horizontalSpacing) / 2
    
    /// The radius of the corners of the section rounded rect.
    static let rectRadius: CGFloat = 15
    
    /// The distance from the left edge of the section to the left edge of the circle.
    static let insetToCircle: CGFloat = 30
    
    /// The distance between the right edge of the circle and the title. Doesn't count the reps circle.
    static let circleToTitle: CGFloat = 4
    
    /// The vertical spacing between the notes field and the text field that holds lyrics.
    static let notesToText: CGFloat = 0
    
    /// The inset away from the edges rounded rect to place text.
    static let textInset: CGFloat = 8
    
    /// The distance between the border and the circle and the title.
    static let borderGap: CGFloat = 8
    
    /// The width for the border. This should be an even number because it is often used as it's half value.
    //static let borderWidth: CGFloat = 10
    
    /// The font size to use for the title. Use a bold font.
    static let titleFontSize: CGFloat = 14
    
    /// The font size to use for the notes field.
    static let notesFontSize: CGFloat = 11
    
    /// The font size to use for the text field that will hold the lyrics.
    static let lyricsFontSize: CGFloat = 11
    
    private(set) var bubbleLayer: SectionCircleLayer
    private(set) var titleLayer: FittingTextLayer
    private(set) var notesLayer: FittingTextLayer
    private(set) var bodyTextLayer: FrameLayer
    private(set) var chordsLayer: ChordsLayer
    private(set) var borderLayer: CALayer
    private(set) var borderGapLayer: CALayer
    
    struct LayerName {
        static let superlayer = "superlayer"
        static let bubble = "Bubble"
        static let title = "Title"
        static let notes = "Notes"
        static let bodyText = "BodyText"
        static let chords = "Chords"
        static let border = "Border"
        static let borderGap = "BorderGap"
    }
    
    init(for section: Section) {
        let width = SectionLayer.width
        let borderThickness: CGFloat = 2
        
        bubbleLayer = SectionCircleLayer(text: section.abriviation, repetitions: section.repetitions)
        bubbleLayer.fillColor = .randomColor()
        bubbleLayer.name = LayerName.bubble
        
        titleLayer = FittingTextLayer.textLayer(withText: section.title,
                                                   font: NSFont(name: Constants.boldFontName, size: SectionLayer.titleFontSize)!,
                                                  color: NSColor.black)
        titleLayer.bounds = titleLayer.calculateBounds()
        titleLayer.name = LayerName.title
        
        notesLayer = FittingTextLayer.textLayer(withText: section.notes,
                                                   font: NSFont(name: Constants.fontName, size: SectionLayer.notesFontSize)!,
                                                  color: .lightGray)
        notesLayer.alignmentMode = convertToCATextLayerAlignmentMode(convertFromCATextLayerAlignmentMode(CATextLayerAlignmentMode.right))
        notesLayer.anchorPoint = CGPoint(x: 1, y: 1)
        notesLayer.bounds = notesLayer.calculateBounds()
        notesLayer.name = LayerName.notes
        
        // TODO: Encapsulate the framesetter logic in FrameLayer
        let attributes = SectionLayer.bodyAttributes
        let attBody = NSAttributedString(string: section.lyrics, attributes: attributes)
        
        let framesetter = Framesetter(attributedString: attBody)
        
        let maxTextWidth = width - SectionLayer.textInset*2 - 2*borderThickness
        let constraintSize = CGSize(width: maxTextWidth, height: CGFloat.greatestFiniteMagnitude)
        let wholeString = NSRange(location: 0, length: 0)
        
        let (suggestedSize, _) = framesetter.suggestedFrameSize(textRange: wholeString, maxSize: constraintSize, frameAttributes: nil)
        let suggestedBounds = CGRect(origin: CGPoint.zero, size: suggestedSize)
        let boundingPath = CGPath(rect: suggestedBounds, transform: nil)
        
        let textFrame = TextFrame(framesetter: framesetter, textRange: wholeString, boundingPath: boundingPath, attributes: nil)
        
        bodyTextLayer = FrameLayer(textFrame)
        bodyTextLayer.bounds = suggestedBounds
        //bodyTextLayer.backgroundColor = NSColor.cyan.blended(withFraction: 0.50, of: NSColor.clear)?.cgColor
        bodyTextLayer.name = LayerName.bodyText
        
        chordsLayer = ChordsLayer(textFrame: textFrame, lyrics: section.lyrics as NSString, chords: section.chordPhrases)
        //chordsLayer.backgroundColor = NSColor.yellow.blended(withFraction: 0.50, of: NSColor.clear)?.cgColor
        chordsLayer.name = LayerName.chords
        
        let bodyAndChordsRect = bodyTextLayer.bounds.union(chordsLayer.bounds)
        chordsLayer.bounds = bodyAndChordsRect
        bodyTextLayer.bounds = bodyAndChordsRect
        
        borderLayer = CALayer()
        borderLayer.borderColor = NSColor.lightGray.cgColor
        borderLayer.borderWidth = borderThickness
        borderLayer.cornerRadius = SectionLayer.rectRadius
        borderLayer.backgroundColor = NSColor.clear.cgColor
        borderLayer.contentsScale = 2.0
        borderLayer.name = LayerName.border
        
        borderGapLayer = CALayer()
        borderGapLayer.backgroundColor = NSColor.white.cgColor
        borderGapLayer.name = LayerName.borderGap
        borderGapLayer.contentsScale = 2.0
        // Width is set with constraints
        borderGapLayer.bounds = NSRect(x: 0, y: 0, width: 0, height: borderThickness)
        
        super.init()
        anchorPoint = CGPoint.zero
        
        //borderWidth = 1
        //borderColor = NSColor.orange.cgColor
        
        addSublayer(borderLayer)
        addSublayer(borderGapLayer)
        addSublayer(bubbleLayer)
        addSublayer(titleLayer)
        addSublayer(notesLayer)
        addSublayer(bodyTextLayer)
        addSublayer(chordsLayer)
        
        // The height of the bubble that goes above the center of the border.
        let bubbleAddedHeight = (bubbleLayer.bounds.size.height - bubbleLayer.mainCircle.frame.center.y)
        
        let height = 2*SectionLayer.textInset + 3*borderLayer.borderWidth/2  + bodyTextLayer.bounds.height + SectionLayer.notesToText + notesLayer.bounds.height + bubbleAddedHeight
        
        let boundsRect = CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: height))
        //let boundsRect = CGRect(x: 0, y: 0, width: maxWidth, height: 100)
        bounds = boundsRect
        
        layoutManager = CAConstraintLayoutManager()
        
        addBorderConstraints()
        addBorderGapConstraints()
        addBubbleConstraints()
        addTitleConstraints()
        addNotesConstraints()
        addBodyTextConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addBorderConstraints() {
        let left = CAConstraint(attribute: .minX, relativeTo: LayerName.superlayer, attribute: .minX, offset: 0)
        let right = CAConstraint(attribute: .maxX, relativeTo: LayerName.superlayer, attribute: .maxX, offset: -0)
        let top = CAConstraint(attribute: .maxY, relativeTo: LayerName.bubble, attribute: .minY, offset: bubbleLayer.mainCircle.frame.center.y + borderLayer.borderWidth/2)
        let bottom = CAConstraint(attribute: .minY, relativeTo: LayerName.superlayer, attribute: .minY, offset: 0)
        borderLayer.constraints = [left, right, top, bottom]
    }
    
    func addBorderGapConstraints() {
        let left = CAConstraint(attribute: .minX, relativeTo: LayerName.bubble, attribute: .minX, offset: -SectionLayer.borderGap)
        let right = CAConstraint(attribute: .maxX, relativeTo: LayerName.title, attribute: .maxX, offset: SectionLayer.borderGap)
        let top = CAConstraint(attribute: .maxY, relativeTo: LayerName.border, attribute: .maxY)
        borderGapLayer.constraints = [left, right, top]
    }
    
    func addBubbleConstraints() {
        let left = CAConstraint(attribute: .minX, relativeTo: LayerName.superlayer, attribute: .minX, offset: SectionLayer.insetToCircle)
        // This might need an extra offset depending on the bounds of the border compared to it's border width. This assumes the bounds only hold the inside half of the border width.
        let top = CAConstraint(attribute: .maxY, relativeTo: LayerName.superlayer, attribute: .maxY)
        bubbleLayer.constraints = [left, top]
    }
    
    func addTitleConstraints() {
        let left = CAConstraint(attribute: .minX, relativeTo: LayerName.bubble, attribute: .maxX, offset: SectionLayer.circleToTitle)
        let centerY = CAConstraint(attribute: .midY, relativeTo: LayerName.border, attribute: .maxY, offset: -borderLayer.borderWidth/2)
        titleLayer.addConstraint(left)
        titleLayer.addConstraint(centerY)
    }
    
    func addNotesConstraints() {
        let right = CAConstraint(attribute: .maxX, relativeTo: LayerName.border, attribute: .maxX, offset: -borderLayer.borderWidth - SectionLayer.textInset)
        let top = CAConstraint(attribute: .maxY, relativeTo: LayerName.border, attribute: .maxY, offset: -borderLayer.borderWidth - SectionLayer.textInset)
        notesLayer.constraints = [right, top]
    }
    
    func addBodyTextConstraints() {
        let left = CAConstraint(attribute: .minX, relativeTo: LayerName.superlayer, attribute: .minX, offset: SectionLayer.textInset + borderLayer.borderWidth)
        let bottom = CAConstraint(attribute: .minY, relativeTo: LayerName.superlayer, attribute: .minY, offset: SectionLayer.textInset + borderLayer.borderWidth)
        
        bodyTextLayer.constraints = [left, bottom]
        chordsLayer.constraints = [left, bottom]
    }
    
    static let bodyAttributes: [NSAttributedString.Key:Any] = {
        let font = NSFont(name: Constants.fontName, size: SectionLayer.lyricsFontSize)!
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = font.pointSize + 2
        return [NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: font]
    }()
}

extension CALayer {
    func callOnAllSublayers(_ closure: (CALayer) -> ()) {
        guard let sublayers = sublayers else { return }
        for sublayer in sublayers {
            closure(sublayer)
            sublayer.callOnAllSublayers(closure)
        }
    }
}

extension FittingTextLayer {
    
    static func textLayer(withText text: String, font: NSFont, color: NSColor) -> FittingTextLayer {
        let layer = FittingTextLayer(maxMagnificationFactor: 4.0, screenContentScale: 2.0)

        layer.alignmentMode = convertToCATextLayerAlignmentMode(convertFromCATextLayerAlignmentMode(CATextLayerAlignmentMode.left))
        layer.anchorPoint = CGPoint(x: 0, y: 0)
        layer.foregroundColor = color.cgColor
        //layer.backgroundColor = NSColor.green.cgColor
        
        layer.string = text
        layer.nsfont = font
        
        return layer
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToCATextLayerAlignmentMode(_ input: String) -> CATextLayerAlignmentMode {
	return CATextLayerAlignmentMode(rawValue: input)
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATextLayerAlignmentMode(_ input: CATextLayerAlignmentMode) -> String {
	return input.rawValue
}
