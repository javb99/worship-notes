//
//  BodyLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/28/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

class BodyLayer: CALayer {
    var sections: [Section]
    var firstSection: Int
    var lastSection: Int
    
    /// Note: This will have a count equal to lastSection - firstSection not sections.count
    var sectionLayers: [SectionLayer]
    
    init(sections: [Section], firstSection: Int, size: CGSize){
        self.sections = sections
        self.firstSection = firstSection
        self.lastSection = firstSection
        sectionLayers = [SectionLayer]()
        super.init()
        anchorPoint = CGPoint(x: 0, y: 0)
        contentsScale = 2.0
        bounds = CGRect(origin: CGPoint.zero, size: size)
        
        //borderColor = NSColor.brown.cgColor
        //borderWidth = 1
        
        var column = 0
        var yFromTop: CGFloat = SectionLayer.verticalSpacing
        for i in firstSection..<sections.count {
            let section = sections[i]
            let sectionLayer = SectionLayer(for: section)
            let height = sectionLayer.bounds.height
            
            // Move to next column.
            if (yFromTop + height) > bounds.height {
                column += 1
                yFromTop = SectionLayer.verticalSpacing
                
                // Move to next page.
                if column == 2 {
                    break // Avoid incrementing lastSection too much.
                    // Currently, stopping means duplicating the first section of every non-first page.
                }
            }
            
            sectionLayer.position = CGPoint(x: CGFloat(column) * (SectionLayer.width + SectionLayer.horizontalSpacing), y: bounds.height - yFromTop - height) // Require bounds to already be set.
            addSublayer(sectionLayer)
            sectionLayers.append(sectionLayer)
            
            yFromTop += height + SectionLayer.verticalSpacing
            
            lastSection = i
        }
        
    }
    
    /// Relative to the begining of sequence.
    /// Point is given in the coordinate space of BodyLayer.
    func sectionIndex(at point: CGPoint) -> Int? {
        for i in 0..<sectionLayers.count {
            let sectionLayer = sectionLayers[i]
            print("Section frame: \(sectionLayer.frame)")
            if sectionLayer.frame.contains(point) {
                return firstSection + i
            }
        }
        return nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
