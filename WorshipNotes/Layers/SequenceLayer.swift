//
//  SequenceLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 12/1/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

class SequenceLayer: CALayer {
    var bubbles: [SectionCircleLayer] = []
    
    init(width: CGFloat, sequence sections: [Section]) {
        super.init()
        anchorPoint = CGPoint(x: 0, y: 0)
        
        // Height is the height of a section circle with reps.
        bounds = CGRect(x: 0, y: 0, width: width, height: SectionCircleLayer.radius*2 + SectionCircleLayer.repRadius)//TODO: CHANGE THIS  This will actually be absolete
        
        let bubbleWidth: CGFloat = SectionCircleLayer.radius*2
        let positionableWidth = width - 2*bubbleWidth
        let widthPer = positionableWidth / CGFloat(sections.count)
        
        
        for i in 0..<sections.count {
            let section = sections[i]
            let sectionCircle = SectionCircleLayer(text: section.abriviation, repetitions: section.repetitions)
            sectionCircle.anchorPoint = CGPoint(x: 0.5, y: 0)
            addSublayer(sectionCircle)
            bubbles.append(sectionCircle)
            
            // Equally space all circles in area given.
            sectionCircle.position = CGPoint(x: bubbleWidth + CGFloat(i) * widthPer + widthPer/2, y: 0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
}
