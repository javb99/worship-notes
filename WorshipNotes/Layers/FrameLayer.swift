//
//  FrameLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/28/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

class FrameLayer: CALayer {
    var textFrame: TextFrame
    
    init(_ textFrame: TextFrame) {
        self.textFrame = textFrame
        super.init()
        //borderColor = NSColor.orange.cgColor
        //borderWidth = 1
        anchorPoint = CGPoint.zero
        contentsScale = 8.0
        setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        
        ctx.setAllowsAntialiasing(true)
        ctx.setAllowsFontSmoothing(true)
        ctx.setAllowsFontSubpixelPositioning(true)
        ctx.setAllowsFontSubpixelQuantization(true)
        
        ctx.setFillColor(.white);
        ctx.fill(bounds);
        
        ctx.setShouldAntialias(true)
        ctx.setShouldSmoothFonts(true)
        ctx.setShouldSubpixelPositionFonts(true)
        ctx.setShouldSubpixelQuantizeFonts(true)
        
        ctx.setShouldSmoothFonts(true);
        textFrame.draw(in: ctx)
        
        ctx.restoreGState()
    }
}
