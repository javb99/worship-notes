//
//  FittingTextLayer.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/18/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

/// Only uses NSFont, don't set font or fontSize using CATextLayer's methods
class FittingTextLayer: MagnifiableTextLayer {
    
    func calculateBounds() -> CGRect {
        guard let string = string as? NSString, let font = font as? NSFont else { return CGRect.zero }
        let size = string.size(withAttributes: [NSAttributedString.Key.font : font])
        return CGRect(x: 0, y: 0, width: ceil(size.width), height: ceil(font.boundingRectForFont.height + font.descender))
    }
    
    override var string: Any? {
        didSet {
            bounds = calculateBounds()
        }
    }
    
    override var font: CFTypeRef? {
        set {
            fatalError("Don't use font! use nsFont.")
        }
        get {
            return super.font
        }
    }
    
    var nsfont: NSFont {
        set {
            super.font = newValue
            fontSize = newValue.fontDescriptor.pointSize
            bounds = calculateBounds()
        }
        
        get {
            guard let font = font as? NSFont else {
                fatalError("FittingTextLayer only works with NSFont.")
            }
            return font
        }
    }
}
