//
//  Layers.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/26/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

/// A layer that holds all the chords. Meant to be placed at the same location as the frame given.
class ChordsLayer: CALayer {
    
    /// The font size to use for the chords. Use a bold font.
    static let fontSize: CGFloat = 11
    
    /// The lyrics the chord indexes are relative to.
    var lyrics: NSString
    
    /// The chord objects to display.
    var chords: [ChordPhrase]
    
    /// The TextFrame that holds lyrics these chords are relative to.
    var textFrame: TextFrame
    
    init(textFrame: TextFrame, lyrics: NSString, chords: [ChordPhrase]) {
        self.lyrics = lyrics
        self.chords = chords
        self.textFrame = textFrame
        super.init()
        anchorPoint = CGPoint.zero
        contentsScale = 2.0
        
        //borderColor = NSColor.green.cgColor
        //borderWidth = 1
        
        guard chords.count > 0 else {
            bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
            return
        }
        
        // Duplicates the body text. TODO: This hack should be fixed.
        let font = NSFont.systemFont(ofSize: SectionLayer.lyricsFontSize)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = font.pointSize + 2
        let attString = NSAttributedString(string: lyrics as String, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: font])
        
        let chordPositions = caculateChordPositions()
        for i in 0..<chordPositions.count {
            var pos = chordPositions[i]
            let chordLayer = createChordLayer(with: chords[i].text)
            chordLayer.name = "A Chord"
            
            let font = attString.attribute(NSAttributedString.Key.font, at: chords[i].index, effectiveRange: nil) as? NSFont
            //pos.y += font!.ascender + abs(font!.descender) + 0
            pos.y += font!.ascender
            
            pos.y = floor(pos.y)
            pos.x = floor(pos.x)
            
            chordLayer.position = pos
            addSublayer(chordLayer)
        }
        
        var maxX: CGFloat = 0
        var maxY: CGFloat = 0
        var minX = CGFloat.greatestFiniteMagnitude
        var minY = CGFloat.greatestFiniteMagnitude
        for chordLayer in sublayers! {
            if chordLayer.frame.maxX > maxX {
                maxX = chordLayer.frame.maxX
            }
            if chordLayer.frame.minX < minX {
                minX = chordLayer.frame.minX
            }
            if chordLayer.frame.maxY > maxY {
                maxY = chordLayer.frame.maxY
            }
            if chordLayer.frame.minY < minY {
                minY = chordLayer.frame.minY
            }
        }
        bounds = CGRect(x: minX, y: minY, width: maxX - minX, height: maxY - minY)
        //print("Chords Layer bounds: \(floor(bounds.width))x\(floor(bounds.height))")
    }
    
    func createChordLayer(with text: String) -> CALayer {
        let textLayer = FittingTextLayer(maxMagnificationFactor: 4.0)
        textLayer.contentsScale = 2.0
        textLayer.anchorPoint = CGPoint(x: 0, y: 0)
        textLayer.foregroundColor = NSColor.blue.cgColor
        textLayer.string = text
        let font = NSFont(name: Constants.boldFontName, size: ChordsLayer.fontSize)!
        textLayer.nsfont = font
        textLayer.alignmentMode = convertToCATextLayerAlignmentMode(convertFromCATextLayerAlignmentMode(CATextLayerAlignmentMode.left))
        // TODO: I think the height is wrong.
        
        //textLayer.borderColor = NSColor.brown.cgColor
        //textLayer.borderWidth = 1
        
        return textLayer
    }
    
    // Requires a chord not be the end of the text. A trailing space is added to lyrics when splitting chords and lyrics.
    func caculateChordPositions() -> [CGPoint] {
        let lines = textFrame.lines
        let lineOrigins = textFrame.lineOriginPoints(forStringRange: NSRange(location: 0, length: lines.count))
        var curLineIndex = 0
        
        var stringIndex = 0
        
        var chordPositions = [CGPoint]()
        for i in 0..<chords.count {
            let curChord = chords[i]
            
//            if curChord.index == 0 {
//                chordPositions.append(lineOrigins[curLineIndex])
//                print("Chord(\(curChord)) at \(0)")
//                continue
//            }
            
            while true {
                guard stringIndex <= curChord.index else { fatalError("Chords were out of order") }
                let curLineRange = lines[curLineIndex].textRange
                if curLineRange.contains(curChord.index) {
                    let offset = lines[curLineIndex].lineOffset(forStringIndex: curChord.index)
                    var chordPosition = lineOrigins[curLineIndex]
                    chordPosition.x += offset
                    chordPositions.append(chordPosition)
                    //print("Chord(\(curChord)) at \(offset)")
                    break
                } else {
                    
                    curLineIndex += 1
                    stringIndex += curLineRange.length
                    
                    guard curLineIndex < lines.count else { fatalError("Going to line that doesn't exist.") }
                }
            }
            
        }
        return chordPositions
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToCATextLayerAlignmentMode(_ input: String) -> CATextLayerAlignmentMode {
	return CATextLayerAlignmentMode(rawValue: input)
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATextLayerAlignmentMode(_ input: CATextLayerAlignmentMode) -> String {
	return input.rawValue
}
