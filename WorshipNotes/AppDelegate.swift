//
//  AppDelegate.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/24/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

struct Constants {
    static let fontName: String = "Arial"
    static let boldFontName: String = "Arial-BoldMT"
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

