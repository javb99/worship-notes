//
//  FooterView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/18/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSNib.Name {
    static let chartPageFooterView = NSNib.Name("PageFooterView")
}

class FooterView: NSView {
    @IBOutlet weak var copyrightLabel: NSTextField!
}
