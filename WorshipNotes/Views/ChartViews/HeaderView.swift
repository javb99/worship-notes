//
//  HeaderView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/18/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSNib.Name {
    static let chartPageHeaderView = NSNib.Name("PageHeaderView")
}

class HeaderView: NSView {
    @IBOutlet weak var titleLabel: NSTextField!
    @IBOutlet weak var authorsLabel: NSTextField!
    @IBOutlet weak var keyLabel: NSTextField!
    @IBOutlet weak var tempoLabel: NSTextField!
    @IBOutlet weak var measureLabel: NSTextField!
    @IBOutlet weak var sequenceView: SequenceView!
}
