//
//  SectionView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/18/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class SectionView: NSView {
    
    var section: Section
    
    init(representedSection: Section) {
        section = representedSection
        
        let sectionLayer = SectionLayer(for: section)
        
        super.init(frame: sectionLayer.bounds)
        
        wantsLayer = true
        layer = sectionLayer
//        sectionLayer.callOnAllSublayers { sublayer in
//            print("Name: \(sublayer.name ?? ""), ContentScale: \(sublayer.contentsScale), position: \(sublayer.position)")
//        }
    }
    
    override func draw(_ dirtyRect: NSRect) {
        guard let ctx = NSGraphicsContext.current, let sectionLayer = sectionLayer else { return }
        if !ctx.isDrawingToScreen {
            let oldBounds = sectionLayer.bounds
            sectionLayer.frame = frame
            sectionLayer.render(in: ctx.cgContext)
            sectionLayer.frame = oldBounds
        }
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var sectionLayer: SectionLayer? {
        return layer as? SectionLayer
    }
}
