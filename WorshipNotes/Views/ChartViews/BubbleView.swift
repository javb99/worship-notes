//
//  BubbleView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/29/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class BubbleView: NSView {
    
    var reps: Int {
        didSet {
            bubbleLayer?.reps = reps
        }
    }
    
    var text: String {
        didSet {
            bubbleLayer?.text = text
        }
    }
    
    var fillColor: NSColor = .white {
        didSet {
            bubbleLayer?.fillColor = fillColor
        }
    }
    
    /// Nil denotes a hidden border. This is the border color to set for highlighting. The text colored border is always visible.
    var stateBorderColor: NSColor? = nil {
        didSet {
            bubbleLayer?.stateBorderColor = stateBorderColor
        }
    }
    
    init(text: String = "", fill fillColor: NSColor = .white, reps: Int = 1) {
        self.text = text
        self.fillColor = fillColor
        self.reps = reps
        
        let bubbleLayer = SectionCircleLayer(text: text, repetitions: reps)
        
        super.init(frame: bubbleLayer.bounds)
        
        wantsLayer = true
        layer = bubbleLayer
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        guard let ctx = NSGraphicsContext.current, let layer = layer else { return }
        if !ctx.isDrawingToScreen {
            let oldBounds = layer.bounds
            layer.frame = frame
            layer.render(in: ctx.cgContext)
            layer.frame = oldBounds
            // TODO: Decide if different fill colors are needed when printing black and white.
        }
    }
    
    var bubbleLayer: SectionCircleLayer? {
        return layer as? SectionCircleLayer
    }
}
