//
//  PageBodyView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

class PageBodyView: NSView, Page {

    /// The vertical spacing between sections. Not added at the top or bottom.
    static let sectionSpacing: CGFloat = 5
    
    /// Horizontal spacing between sections.
    static let columnSpacing: CGFloat = ChartPageView.margins.left
    
    /// How many columns of sections to use.
    let numberOfColumns: Int
    
    var maxColumnHeight: CGFloat {
        didSet {
            for column in  columns {
                column.maxHeight = maxColumnHeight
            }
        }
    }
    
    var columns: [ColumnItemsContainer] = []
    var currentFillingColumn = 0
    
    var sectionViews: [SectionView] = []
    
    init(numberOfColumns: Int = 2, maxColumnHeight: CGFloat = 0) {
        self.numberOfColumns = numberOfColumns
        self.maxColumnHeight = maxColumnHeight
        
        super.init(frame: .zero)
        
        for _ in 0..<numberOfColumns {
            let column = ColumnItemsContainer()
            //column.maxHeight = maxColumnHeight
            columns.append(column)
        }
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func add(_ item: ItemView) {
        if columns[currentFillingColumn].canFit(item) {
            columns[currentFillingColumn].add(item)
            addSubview(item)
        } else {
            let nextColumnIndex = currentFillingColumn + 1
            
            if columns[nextColumnIndex].canFit(item) {
                columns[nextColumnIndex].add(item)
                addSubview(item)
                currentFillingColumn = nextColumnIndex
            } else {
                fatalError("Trying to add a section that does not fit in Body") // TODO: Pass to user.
            }
        }
    }
    
    func canFit(_ item: ItemView) -> Bool {
        if columns[currentFillingColumn].canFit(item) {
            // Fits in the current section.
            return true
        } else if currentFillingColumn < numberOfColumns-1 {
            if columns[currentFillingColumn + 1].canFit(item) {
                // Fits in next section.
                return true
            } else {
                print("OVERSIZED SECTION: Didn't fit in an empty column.") // TODO: Pass to user.
                return false
            }
        } else {
            // No More containers.
            return false
        }
    }
    
    override func layout() {
        super.layout()
        
        let totalSpacing = PageBodyView.columnSpacing * CGFloat(numberOfColumns-1)
        let columnWidth = (frame.width - totalSpacing) / CGFloat(numberOfColumns)
        
        for (i, column) in columns.enumerated() {
            column.maxHeight = bounds.height
            let columnOffset = CGPoint(x: columnWidth * CGFloat(i) + PageBodyView.columnSpacing * CGFloat(i), y: 0)
            for (j, sectionView) in column.itemViews.enumerated() {
                sectionView.frame = NSRect(x: columnOffset.x, y: columnOffset.y + column.yInContainer[j], width: sectionView.bounds.width, height: sectionView.bounds.height)
                //assert(bounds.contains(sectionView.frame))
            }
        }
    }
}

/// A SectionContainer that lays sections out in a column.
class ColumnItemsContainer {

    var maxHeight: CGFloat = 0
    
    var itemViews: [ItemView] = []
    var yInContainer: [CGFloat] = []
    
    var filledHeight: CGFloat = 0
    var isEmpty: Bool {
        return filledHeight == 0
    }
    var heightEmpty: CGFloat {
        return maxHeight - filledHeight
    }
    
    /// Adds the sectionView and stores its y position in container space.
    func add(_ itemView: ItemView) {
        itemViews.append(itemView)
        
        let interSectionGap = !isEmpty ? PageBodyView.sectionSpacing : 0
        let addedHeight = interSectionGap + itemView.bounds.height
        yInContainer.append(heightEmpty - addedHeight)
        
        filledHeight += addedHeight
    }
    
    func canFit(_ itemView: ItemView) -> Bool {
        guard maxHeight > 0 else { return false }
        
        let interSectionGap = !isEmpty ? PageBodyView.sectionSpacing : 0
        
        if (interSectionGap + itemView.frame.height) < heightEmpty  {
            // The section will fit.
            return true
        } else {
            // The section won't fit.
            return false
        }
    }
}
