//
//  ChartPageView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/28/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSView {
    /// Instantiates the nib and returns the first View in the top level objects.
    static func instantiate(fromNibNamed name: NSNib.Name, inBundle bundle: Bundle? = nil, withOwner owner: Any? = nil) -> NSView? {
        var topObjects: NSArray?
        NSNib(nibNamed: name, bundle: bundle)?.instantiate(withOwner: owner, topLevelObjects: &topObjects)
        
        guard topObjects != nil else { return nil }
        for object in topObjects! {
            switch object {
            case let view as NSView:
                return view
            default:
                break
            }
        }
        return nil
    }
}

class ChartPageView: NSView {
    
    /// The width of a page.
    static let width: CGFloat = 612
    
    /// The height of a page.
    static let height: CGFloat = 792
    
    /// The bounds of a page with the origin at (0,0).
    static let page: CGRect = CGRect(x: 0, y: 0, width: width, height: height)
    
    /// The insets of a page that should not have objects placed in them.
    static let margins: NSEdgeInsets = NSEdgeInsets(top: 35, left: 35, bottom: 35, right: 35)
    
    /// The usable area of the page.
    static let usablePage: CGRect = page.inset(by: margins)
    
    var header: HeaderView
    var body: PageBodyView
    var footer: FooterView
    
    init() {
        header = NSView.instantiate(fromNibNamed: .chartPageHeaderView, inBundle: nil, withOwner: nil) as! HeaderView
        header.translatesAutoresizingMaskIntoConstraints = false
        
        footer = NSView.instantiate(fromNibNamed: .chartPageFooterView, inBundle: nil, withOwner: nil) as! FooterView
        footer.translatesAutoresizingMaskIntoConstraints = false
        
        body = PageBodyView(numberOfColumns: 2)
        body.translatesAutoresizingMaskIntoConstraints = false
        
        super.init(frame: NSRect(origin: CGPoint.zero, size: ChartPageView.page.size))
        
        addBoundingBox(constrainedTo: self, fillColor: .white)
        //addBoundingBox(constrainedTo: header, fillColor: NSColor.orange.transparent!)
        //addBoundingBox(constrainedTo: footer, fillColor: NSColor.red.transparent!)
        //addBoundingBox(constrainedTo: body, fillColor: NSColor.cyan.transparent!)
        
        addSubview(header)
        addSubview(body)
        addSubview(footer)

        addConstraints()
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addConstraints() {
        let margins = ChartPageView.margins
        
        header.constrain(.left, to: self, constant: margins.left)
        header.constrain(.right, to: self, constant: -margins.right)
        header.constrain(.top, to: self, constant: margins.top)

        footer.constrain(.left, to: header)
        footer.constrain(.right, to: header)
        footer.constrain(.bottom, to: self, constant: -margins.bottom)
        
        body.constrain(.left, to: header)
        body.constrain(.right, to: header)
        body.constrain(.top, to: header, attribute: .bottom)
        body.constrain(.bottom, to: footer, attribute: .top)
    }
}

extension ChartPageView: Page {
    
    func canFit(_ item: ItemView) -> Bool {
        return body.canFit(item)
    }
    
    func add(_ item: ItemView) {
        body.add(item)
    }
}
