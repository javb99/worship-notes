//
//  PaginatedCollectionView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 8/2/18.
//  Copyright © 2018 Joseph Van Boxtel. All rights reserved.
//

import Cocoa

/// Provides access to SectionViews
protocol ItemSource {
    
    func itemView(for index: Int) -> ItemView?
    
    func numberOfItemViews() -> Int
}

typealias ItemView = NSView

/// Provides access to SectionViews
protocol PageSource {
    
    func createPage(for index: Int) -> PageView
}

protocol Page {
    
    func add(_ section: ItemView)
    
    func canFit(_ section: ItemView) -> Bool
}

typealias PageView = Page & NSView


/// Any Array of ItemViews can serve as a basic ItemSource
extension Array: ItemSource where Element: ItemView {
    
    func itemView(for index: Int) -> ItemView? {
        return self[index]
    }
    
    func numberOfItemViews() -> Int {
        return self.count
    }
}


class PaginatedCollectionView: NSView {
    
    public var itemSource: ItemSource?
    public var pageSource: PageSource?
    
    private(set) var pages: [PageView] = []
    
    public func add(_ pageView: PageView) {
        pageView.frame = frameRect(forPageViewOfSize: pageView.frame.size, at: pages.endIndex)
        
        pages.append(pageView)
        addSubview(pageView)
        
        frame = NSRect(origin: frame.origin, size: intrinsicContentSize)
    }
    
    public func removePage(at index: Int) {
        precondition(pages.isIndexValid(index), "Index out of bounds.")
        
        let pageView = pages.remove(at: index)
        pageView.removeFromSuperview()
    }
    
    public func removeAllPages() {
        for i in pages.indices.reversed() {
            removePage(at: i)
        }
    }
    
    static let pageSpacing: CGFloat = 10
    
    func frameRect(forPageViewOfSize size: NSSize, at index: Int) -> NSRect {
        return NSRect(x: (size.width + PaginatedCollectionView.pageSpacing) * CGFloat(index), y: 0, width: size.width, height: size.height)
    }
    
    override var intrinsicContentSize: NSSize {
        /// Find the union of all page frames and return that size.
        let size = pages.map{ $0.frame }.reduce(NSRect.zero, { $0.union($1) }).size
        return size
    }
    
    // MARK: Section Layout
    
    public func reloadItems() {
        removeAllPages()
        
        guard let pageSource = pageSource else {
            print("No pageSource given")
            return
        }
        
        guard let itemSource = itemSource else {
            print("No itemSource given")
            return
        }
        
        add(pageSource.createPage(for: 0))
        
        var containerIndex = 0
        
        let count = itemSource.numberOfItemViews()
        for i in 0..<count {
            guard let itemView = itemSource.itemView(for: i) else {
                print("nil returned by itemView(for: \(i))!")
                return
            }
            
            if pages[containerIndex].canFit(itemView) {
                pages[containerIndex].add(itemView)
            } else {
                let page = pageSource.createPage(for: pages.endIndex)
                
                if page.canFit(itemView) {
                    page.add(itemView)
                    add(page)
                    containerIndex += 1
                } else {
                    fatalError("OVERSIZED SECTION: Did not fit in an empty page.")
                    break
                }
            }
        }
    }
}
