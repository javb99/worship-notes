//
//  ChartView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/17/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

struct ChartPageSource: PageSource {
    
    var songMeta: SongMeta
    
    func createPage(for index: Int) -> PageView {
        let pageView = ChartPageView()
        
        pageView.setFields(using: songMeta)
        
        pageView.constrainAspectRatio(8.5/11)
        pageView.constrain(.width, to: pageView.bounds.width)
        
        // This lays out the page. So that it knows how big it's section area is.
        pageView.needsLayout = true
        pageView.layoutSubtreeIfNeeded()
        
        return pageView
    }
}

/// A superclass for views that want to display a song chart.
class ChartView: PaginatedCollectionView {
    
    override func knowsPageRange(_ range: NSRangePointer) -> Bool {
        range.pointee.length = pages.count
        range.pointee.location = 1
        return true
    }
    
    override func rectForPage(_ page: Int) -> NSRect {
        // Convert from 1-based PDF pages to 0-based page array.
        let convertedIndex = page - 1
        precondition(pages.isIndexValid(convertedIndex), "Index out of bounds")
        
        return frameRect(forPageViewOfSize: ChartPageView.page.size, at: convertedIndex)
    }
}
