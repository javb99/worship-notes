//
//  CenteringClipView.swift
//  DocEx
//
//  Created by Joseph Van Boxtel on 1/21/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa


/**
 
 `CenteringClipView` is a clip view subclass that centers smaller document views
 
 within its inset clip bounds (as described by the set `contentInsets`).
 
 */
class CenteringClipView: NSClipView {
    
    var centersDocumentView: Bool
    
    override init(frame frameRect: NSRect) {
        centersDocumentView = true
        super.init(frame: frameRect)
    }
    
    required init?(coder decoder: NSCoder) {
        centersDocumentView = true
        super.init(coder: decoder)
    }
    
    override func constrainBoundsRect(_ proposedBounds: NSRect) -> NSRect {
        var newBounds = super.constrainBoundsRect(proposedBounds)

        guard let documentView = documentView else { return newBounds }
        
        var insets = contentInsets.scaled(by: (newBounds.width / bounds.width))
        
        if documentView.isFlipped { insets = insets.flippedVertically() }
        
        let outsetDocFrame = documentView.frame.outset(by: insets)
        
        // If proposed clip view bounds width is greater than document view frame width, center it horizontally.
        if centersDocumentView && (newBounds.size.width >= outsetDocFrame.size.width) {
            newBounds.origin.x = outsetDocFrame.minX + (outsetDocFrame.size.width - newBounds.size.width) / 2.0
            
        } else if newBounds.width < outsetDocFrame.width {
            // Make sure that all of newBounds is showing the document.
            
            if newBounds.maxX > outsetDocFrame.maxX {
                // The clip rect is outside the maxX edge of the document, bring it in.
                newBounds.origin.x = outsetDocFrame.maxX - newBounds.width
                
            } else if newBounds.minX < outsetDocFrame.minX {
                // The clip rect is outside the minX edge of the document, bring it in.
                newBounds.origin.x = outsetDocFrame.minX
            }
        }
        
        // If proposed clip view bounds height is greater than document view frame height, center it vertically.
        if centersDocumentView && (newBounds.size.height >= outsetDocFrame.size.height) {
            newBounds.origin.y = outsetDocFrame.minY + (outsetDocFrame.size.height - newBounds.size.height) / 2.0
            
        } else if newBounds.width < outsetDocFrame.width {
            // Make sure that all of newBounds is showing the document.
            
            if newBounds.maxY > outsetDocFrame.maxY {
                // The clip rect is outside the maxY edge of the document, bring it in.
                newBounds.origin.y = outsetDocFrame.maxY - newBounds.height
                
            } else if newBounds.minY < outsetDocFrame.minY {
                // The clip rect is outside the minY edge of the document, bring it in.
                newBounds.origin.y = outsetDocFrame.minY
            }
        }
        
        // Round off measurements. Required by ClipView to avoid blurring.
        return backingAlignedRect(newBounds, options: .alignAllEdgesNearest)
    }
}
