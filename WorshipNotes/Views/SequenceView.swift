//
//  SequenceView.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 12/1/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

/// A View wrapper for Sequence Layer.
class SequenceView: NSView {
    
    var sequenceLayer: SequenceLayer? {
        return layer as? SequenceLayer
    }
    
    var sequence: [Section] = [] {
        didSet {
            layer = SequenceLayer(width: bounds.width, sequence: sequence)
        }
    }
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        wantsLayer = true
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        wantsLayer = true
    }
    
    override func layout() {
        //guard let sequence = sequence else { return }
        layer = SequenceLayer(width: bounds.width, sequence: sequence)
        bounds = layer!.bounds
    }
    
    override var intrinsicContentSize: NSSize {
        return NSSize(width: (SectionCircleLayer.radius*2) * CGFloat(sequence.count), height: (SectionCircleLayer.radius*2 + SectionCircleLayer.repRadius))//TODO: CHANGE THIS This will not be needed with the new CollectionView based approach that is coming.
    }
    
    /// Relative to the begining of sequence.
    /// Point is given in the coordinate space of BodyLayer.
    func sectionIndex(at point: CGPoint) -> Int? {
        guard let sequenceLayer = sequenceLayer else { return nil }
        
        for i in 0..<sequence.count {
            let bubble = sequenceLayer.bubbles[i]
            //print("Section Bubble frame: \(bubble.frame)")
            if bubble.frame.contains(point) {
                return i
            }
        }
        return nil
    }
}
