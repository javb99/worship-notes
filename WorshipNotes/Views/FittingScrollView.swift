//
//  FittingScrollView.swift
//  DocEx
//
//  Created by Joseph Van Boxtel on 1/26/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

/// A delegate to customize the size that is scaled to fit by FittingScrollView.
public protocol FittingDelegate {
    /// Return the size that should be scaled to fit in the FittingScrollView.
    func scrollViewSizeToFit(_ scrollView: FittingScrollView) -> NSSize?
}

public protocol ScrollViewLiveUpdatesDelegate {
    /// Sent when the magnification changes on FittingScrollView. This is not sent when the magnification bounces when going past the bounds. In that case the last value returned will be outside the bounds of min and max magnification.
    func scrollView(_ scrollView: FittingScrollView, magnificationChanged magnification: CGFloat)
}

/// A subclass of NSScrollView that can scale its documentView to fit within the visible rectangle. Use documentFitDimension to control the behavior.
public class FittingScrollView: NSScrollView {
    
    public var fittingDelegate: FittingDelegate?
    public var liveUpdatesDelegate: ScrollViewLiveUpdatesDelegate?
    
    /// What dimensions of the documentView(or size given by fittingDelegate) to make fit within the visible rect.
    public enum DocumentViewFitDimension: Int {
        /// Don't magnify the documentView to fit.
        case none
        /// Magnify the documentView so that the entire documentView(or size given by fittingDelegate) fits.
        case both
        /// Magnify the documentView so that the width fits.
        case width
        /// Magnify the documentView so that the height fits.
        case height
    }
    
    /// The dimension that the documentView(or size given by fittingDelegate) will be magnified to fit the content size of the ScrollView. See DocumentViewFitDimension for possible values.
    public var documentFitDimension: DocumentViewFitDimension = .height
    
    /// Update the receiver so the document view is scaled to match the value of documentFitDimension.
    public func magnifyDocumentViewToFit(animated useAnimation: Bool) {
        guard let documentView = documentView else { return }
        guard documentFitDimension != .none else { return }
        
        let sizeToFit = fittingDelegate?.scrollViewSizeToFit(self) ?? documentView.bounds.size
        let clipBounds = contentView.bounds
        
        let widthFactor = (clipBounds.width / sizeToFit.width)
        let heightFactor = (clipBounds.height / sizeToFit.height)
        
        var newMagnification: CGFloat
        
        switch documentFitDimension {
        case .both:
            // Pick the factor that is zoomed out the farthest to show the whole document.
            newMagnification = magnification * min(widthFactor, heightFactor)
        case .width:
            newMagnification = magnification * widthFactor
        case .height:
            newMagnification = magnification * heightFactor
        case .none: // Won't happen. Caught above by guard.
            newMagnification = magnification
        }
        
        //print("SizeToFit: \(sizeToFit) -> magnification: \(newMagnification)")
        
        if useAnimation {
            let animated = self.animator()
            animated.magnification = newMagnification
        } else {
            self.magnification = newMagnification
        }
    }
    
    /// Uses this for to scale the document view the first time it is set.
    @objc func documentViewFrameChanged(_ notification: Notification) {
        magnifyDocumentViewToFit(animated: true)
    }
    
    // MARK: - ScrollView Overrides -
    
    public override var documentView: NSView? {
        didSet {
            guard documentView != nil else { return }
            //magnifyDocumentViewToFit(animated: true)
            postsFrameChangedNotifications = true
            NotificationCenter.default.addObserver(self, selector: #selector(documentViewFrameChanged(_:)), name: NSView.frameDidChangeNotification, object: documentView)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override public var magnification: CGFloat {
        didSet {
            liveUpdatesDelegate?.scrollView(self, magnificationChanged: magnification)
        }
    }
    
    override public func magnify(with event: NSEvent) {
        super.magnify(with: event)
        liveUpdatesDelegate?.scrollView(self, magnificationChanged: magnification)
    }
    
    override public func setFrameSize(_ newSize: NSSize) {
        super.setFrameSize(newSize)
        magnifyDocumentViewToFit(animated: false)
    }
}
