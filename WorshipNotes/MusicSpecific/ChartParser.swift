//
//  ChartParser.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/30/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

enum ChartParsingError: Error {
    /// This is thrown when two opening or two closing brackets are found.
    case doubleBrackets
}

class ChartParser {
    var song: Song?
    
    static func chordsAndLyrics(fromChordPro chordPro: String) throws -> ([ChordPhrase], String) {
        var lyrics = ""
        var chords = [ChordPhrase]()
        var insideBrackets = false
        
        var currentChordStart: String.Index? = nil
        var partialChord = ""
        for i in chordPro.indices {
            let char = chordPro[i]
            if char == "[" {
                guard insideBrackets == false else {
                    throw ChartParsingError.doubleBrackets
                }
                insideBrackets = true
                currentChordStart = lyrics.endIndex
                partialChord = ""
            } else if char == "]" {
                guard insideBrackets == true else {
                    throw ChartParsingError.doubleBrackets
                }
                insideBrackets = false
                let index = currentChordStart!.samePosition(in: lyrics.utf16)?.encodedOffset
                chords.append(ChordPhrase(index: index!, text: partialChord))
            } else {
                if !insideBrackets {
                    lyrics.append(char)
                } else {
                    partialChord.append(char)
                }
            }
        }
        lyrics += " " // A hack to fix a chord showing up at the end.
        
        return (chords, lyrics)
    }
}

struct ChordPhrase: Codable {
    
    /// The index in the lyrics string of where the chord should be displayed above.
    var index: Int
    
    /// The text to display for this chord.
    var text: String
    
    func transposed(from oldKey: MusicalNote, to newKey: MusicalNote) -> ChordPhrase {
        var newText = ""
        
        // Find all note letters followed by sharp or flat symbols.
        let notesExpression = try! NSRegularExpression(pattern: "[ABCDEFG][#|b]*", options: NSRegularExpression.Options(rawValue: 0))
        let matches = notesExpression.matches(in: text, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSRange(location: 0, length: text.utf16.endIndex.encodedOffset))
        
        var endOfLastChord: Int = 0
        var nsText = (text as NSString)
        
        for match in matches {
            // Add non note characters
            newText += nsText.substring(to: match.range.location - endOfLastChord)
            
            // Shift the string to the next note
            nsText = nsText.substring(from: match.range.location - endOfLastChord) as NSString
            
            // Note is at the front of the string
            let range = NSRange(location: 0, length: match.range.length)
            let symbol = nsText.substring(with: range)
            guard let note = MusicalNote(textSymbol: symbol) else {
                break
            }
            
            // Transpose
            let transposedNote = note.transposed(from: oldKey, to: newKey)
            newText += transposedNote.textSymbol
            
            // Shift the string to after the current note
            nsText = nsText.substring(from: match.range.length) as NSString
            endOfLastChord = match.range.upperBound
        }
        
        // Add non note characters at the end of the phrase.
        newText += nsText as String
        
        return ChordPhrase(index: index, text: newText)
    }
}


