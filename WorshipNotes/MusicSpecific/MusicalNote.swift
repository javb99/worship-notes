//
//  MusicalNote.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 1/15/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Foundation

//enum Note: String, Codable {
//    case aFlat = "Ab"
//    case a = "A"
//    case aSharp = "A#"
//    case bFlat = "Bb"
//    case b = "B"
//    case c = "C"
//    case cSharp = "C#"
//    case dFlat = "Db"
//    case d = "D"
//    case dSharp = "D#"
//    case eFlat = "Eb"
//    case e = "E"
//    case f = "F"
//    case fSharp = "F#"
//    case gFlat = "Gb"
//    case g = "G"
//    case gSharp = "G#"
//
//    var semitonesToC: Int {
//        switch self {
//        case .c:
//            return 0
//        case .cSharp, .dFlat:
//            return 1
//        case .d:
//            return 2
//        case .dSharp, .eFlat:
//            return 3
//        case .f:
//            return 4
//        default:
//            fatalError("Not implemented yet")
//        }
//    }
//}

enum MusicalLetter: String, Codable {
    case c = "C"
    case d = "D"
    case e = "E"
    case f = "F"
    case g = "G"
    case a = "A"
    case b = "B"
    
    /// A letter that is the given distance from C. Handles distances greater than 7 and positive and negative distances.
    init?(distanceFromC: Int) {
        var (_, remainder) = distanceFromC.quotientAndRemainder(dividingBy: 7)
        if remainder < 0 { remainder += 7 }
        switch remainder {
        case 0:
            self = .c
        case 1:
            self = .d
        case 2:
            self = .e
        case 3:
            self = .f
        case 4:
            self = .g
        case 5:
            self = .a
        case 6:
            self = .b
        default:
            return nil
        }
    }
    
    /// C is 0. B is 6. Range: [0,6]
    var distanceFromC: Int {
        switch self {
        case .c:
            return 0
        case .d:
            return 1
        case .e:
            return 2
        case .f:
            return 3
        case .g:
            return 4
        case .a:
            return 5
        case .b:
            return 6
        }
    }
    
    /// The letter distance between letters. Range is [-3,3]
    func distance(to: MusicalLetter) -> Int {
        var distance = to.distanceFromC - self.distanceFromC
        if distance < -3 {
            distance += 7
        } else if distance > 3 {
            distance -= 7
        }
        return distance
    }
    
    /// The letter that is offset away from the receiver.
    func offsetBy(_ offset: Int) -> MusicalLetter {
        var (_, newLetterFromC) = (self.distanceFromC + offset).quotientAndRemainder(dividingBy: 7)
        if newLetterFromC < 0 { newLetterFromC += 7 }
        return MusicalLetter(distanceFromC: newLetterFromC)!
    }
}

public struct MusicalNote: Codable {
    
    /// The number of semitones up from C to get to the reciever.
    var semitonesToC: Int
    
    /// The letter for the reciever.
    var baseLetter: MusicalLetter
    
    /// The number of flats to add to the base letter.
    var flats: Int = 0
    
    /// The number of sharps to add to the base letter.
    var sharps: Int = 0
    
    private static func semitones(for baseLetter: MusicalLetter) -> Int {
        switch baseLetter {
        case .c:
            return 0
        case .d:
            return 2
        case .e:
            return 4
        case .f:
            return 5
        case .g:
            return 7
        case .a:
            return 9
        case .b:
            return 11
        }
    }
    
    /// Create a note with the semitone distance up from C and the base letter.
    init(semitones: Int, baseLetter: MusicalLetter) {
        semitonesToC = semitones
        self.baseLetter = baseLetter
        
        /// Add sharps or flats as needed.
        let letterSemis = MusicalNote.semitones(for: baseLetter)
        if letterSemis > semitones {
            flats = letterSemis-semitones
        }
        if letterSemis < semitones {
            sharps = semitones-letterSemis
        }
    }
    
    /// Create a note with only the base letter. It is assumed that there are no sharps or flats.
    init(baseLetter: MusicalLetter) {
        self.init(semitones: MusicalNote.semitones(for: baseLetter), baseLetter: baseLetter)
    }
    
    /// Create a musical note from its symbol. Use '#' for sharp and 'b' for flat. Can handle symbols with more than one flat or sharp.
    init?(textSymbol: String) {
        let lessSharps = textSymbol.replacingOccurrences(of: "#", with: "")
        let sharps = textSymbol.count - lessSharps.count
        let lessSharpsAndFlats = lessSharps.replacingOccurrences(of: "b", with: "")
        let flats = lessSharps.count - lessSharpsAndFlats.count
        guard let baseLetter = MusicalLetter(rawValue: lessSharpsAndFlats) else {
            return nil
        }
        self.init(baseLetter: baseLetter)
        guard !(sharps > 0 && flats > 0) else { return nil }
        for _ in 0..<sharps {
            sharpen()
        }
        for _ in 0..<flats {
            flatten()
        }
    }
    
    static let c = MusicalNote(baseLetter: .c)
    static let d = MusicalNote(baseLetter: .d)
    static let e = MusicalNote(baseLetter: .e)
    static let f = MusicalNote(baseLetter: .f)
    static let g = MusicalNote(baseLetter: .g)
    static let a = MusicalNote(baseLetter: .a)
    static let b = MusicalNote(baseLetter: .b)
    
    var textSymbol: String {
        var text = baseLetter.rawValue
        for _ in 0..<sharps {
            text += "#"
        }
        for _ in 0..<flats {
            text += "b"
        }
        return text
    }
    
    /// Return a note with an additional flat.
    func flat() -> MusicalNote {
        var new = self
        if new.sharps > 0 {
            new.sharps -= 1
        } else {
            new.flats += 1
        }
        new.semitonesToC -= 1
        return new
    }
    
    /// Return a note with an additional sharp.
    func sharp() -> MusicalNote {
        var new = self
        if new.flats > 0 {
            new.flats -= 1
        } else {
            new.sharps += 1
        }
        new.semitonesToC += 1
        return new
    }
    
    /// Add a flat to the note.
    mutating func flatten() {
        if sharps > 0 {
            sharps -= 1
        } else {
            flats += 1
        }
        semitonesToC -= 1
    }
    
    /// Add a sharp to the note.
    mutating func sharpen() {
        if flats > 0 {
            flats -= 1
        } else {
            sharps += 1
        }
        semitonesToC += 1
    }
    
    /// The distance around the semitone circle between two positions. Inputs can be [-11, 11], output will be [-5,6]
    /// Biased positive. ie -6 one way and 6 the other will choose 6.
    func distance(from: Int, to: Int) -> Int {
        var distance = to - from
        if distance <= -6 {
            distance += 12
        } else if distance > 6 {
            distance -= 12
        }
        return distance
    }
    
    /// Semitones to the given note. Always with 6 semitones [-6,6]
    func semitones(to: MusicalNote) -> Int {
        return distance(from: self.semitonesToC, to: to.semitonesToC)
    }
    
    /// Transposes to the equivalent note in newKey. Sharps and flats should match what would be expected in the new key.
    public func transposed(from oldKey: MusicalNote, to newKey: MusicalNote) -> MusicalNote {
        
        // 1. Find the distance between the current key's letter name and the new key's letter name in the list of all letters (not including sharps or flats)
        let keyLetterDistance = oldKey.baseLetter.distance(to: newKey.baseLetter)
        
        // 2. Find the distance in semitones between the current key and the new key.
        let keySemiDistance = oldKey.semitones(to: newKey)
        
        // 3. The new note's letter will be the letterDistance above the current note.
        var newNote = MusicalNote(baseLetter: self.baseLetter.offsetBy(keyLetterDistance))
        
        // 4. Add sharps or flats to the new note's letter until it is the correct number of semitones above the current note.
        var newSemis = self.semitones(to: newNote)
        
        var difference = distance(from: newSemis, to: keySemiDistance)
        while difference != 0{
            guard difference <= 3 else { fatalError("Distance Greater than 3") }
            
            if difference < 0 {
                newNote = newNote.flat()
            }
            if difference > 0 {
                newNote = newNote.sharp()
            }

            newSemis = self.semitones(to: newNote)
            difference = distance(from: newSemis, to: keySemiDistance)
        }
        return newNote
    }
}

extension MusicalNote: Hashable {
    public func hash(into hasher: inout Hasher) {
        semitonesToC.hash(into: &hasher)
        sharps.hash(into: &hasher)
    }
}

extension MusicalNote: Equatable {
    public static func ==(lhs: MusicalNote, rhs: MusicalNote) -> Bool {
        // Only need to check semitones and sharps because flats are always equal if sharps and semitones are.
        return lhs.semitonesToC == rhs.semitonesToC && lhs.sharps == rhs.sharps
    }
}
