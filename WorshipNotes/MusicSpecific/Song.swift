//
//  Section.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/24/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Foundation

struct SongMeta: Codable {
    /// The title of the song.
    var title: String
    
    /// The author or authors of the song.
    var authors: String?
    
    /// The key of the song.
    var key: MusicalNote
    
    /// The tempo of the song in Beats Per Minute(BPM).
    var tempo: Double?
    
    /// The time signature of the song.
    var measure: String?
    
    /// The copyright information for the song. This is usually multiple lines.
    var copyright: String?
}

struct Song: Codable {
    
    var meta: SongMeta
    
    /// The list of sections of the song.
    var sequence: [Section]
    
    /// Transpose all of the chords in the song to the new key.
    func transposed(to newKey: MusicalNote) -> Song {
        var sections: [Section] = []
        for section in sequence {
            sections.append(section.transposed(from: meta.key, to: newKey))
        }
        var newMeta = meta
        newMeta.key = newKey
        return Song(meta: newMeta, sequence: sections)
    }
}

extension Song {
    
    /// A sequence that sums the repetitions of adjacent sections with the same abriviation to create one section bubble.
    var consolidatedSequence: [Section] {
        var mergedSections = [Section]()
        guard sequence.count > 1 else { return sequence }
        
        var lastSection = sequence[0]
        var reps = lastSection.repetitions
        for section in sequence[1...] {
            if section.abriviation != lastSection.abriviation {
                lastSection.repetitions = reps
                reps = 0
                mergedSections.append(lastSection)
            }
            reps += section.repetitions
            lastSection = section
        }
        lastSection.repetitions = reps
        mergedSections.append(lastSection)
        return mergedSections
    }
}

struct Section: Codable, Equatable {
    
    /// The abriviation for the section title to use as the bubble text.
    var abriviation: String
    
    /// The title of the section.
    var title: String
    
    /// The number of times to repeat this section before moving on.
    var repetitions: Int = 1
    
    /// Notes on how to play this section of the song. Currently this is limited to one line of text.
    var notes: String = ""
    
    /// The raw ChordPro text for this section.
    var rawText: String = "" {
        didSet {
            // TODO: Pass this error to the user.
            if let (chords, words) = try? ChartParser.chordsAndLyrics(fromChordPro: rawText) {
                lyrics = words
                chordPhrases = chords
            } else {
                lyrics = rawText
                chordPhrases = []
            }
        }
    }
    
    /// The lyrics for this section. This is obtained by removing the chords.
    var lyrics: String = ""
    
    /// A chunk of chords that are relative to an index in the lyrics string.
    var chordPhrases: [ChordPhrase] = []
    
    init(abriviation: String, title: String) {
        self.abriviation = abriviation
        self.title = title
    }
    
    /// Transpose all the chords inside the chordPhrases to their equivalent in the new key.
    func transposed(from oldKey: MusicalNote, to newKey: MusicalNote) -> Section {
        var transposedChords: [ChordPhrase] = []
        for chordPhrase in chordPhrases {
            transposedChords.append(chordPhrase.transposed(from: oldKey, to: newKey))
        }
        var section = Section(abriviation: abriviation, title: title)
        section.rawText = Section.rawText(from: lyrics, and: transposedChords)
        section.notes = notes
        section.repetitions = repetitions
        return section
    }
    
    static func rawText(from lyrics: String, and chords: [ChordPhrase]) -> String {
        let nsLyrics = lyrics as NSString
        var raw = ""
        var lastIndex: Int = 0
        for phrase in chords {
            raw += nsLyrics.substring(with: NSRange(location: lastIndex, length: phrase.index-lastIndex))
            raw += "[" + phrase.text + "]"
            lastIndex = phrase.index
        }
        if lastIndex < nsLyrics.length-1 { // Add characters after the last chord.
            raw += nsLyrics.substring(with: NSRange(location: lastIndex, length: nsLyrics.length-1 - lastIndex))
        }
        return raw
    }
    
    static func ==(lhs: Section, rhs: Section) -> Bool {
        if lhs.abriviation != rhs.abriviation { return false }
        if lhs.title != rhs.title { return false }
        if lhs.repetitions != rhs.repetitions { return false }
        if lhs.notes != rhs.notes { return false }
        if lhs.rawText != rhs.rawText { return false }
        return true
    }
}

extension Section: Hashable {
    
    func hash(into hasher: inout Hasher) {
        rawText.hash(into: &hasher)
        title.hash(into: &hasher)
        abriviation.hash(into: &hasher)
        notes.hash(into: &hasher)
        repetitions.hash(into: &hasher)
    }
}
