//
//  ChartDocumentExporter.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 2/16/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa
import Quartz

extension NSNib.Name {
    static let exporterAccessoryView = NSNib.Name("ExporterAccessoryView")
}

extension MusicalNote {
    /// The text symbol with 'b's and '#'s replaced with "Flat" and "Sharp" respectively.
    var urlSafeSymbol: String {
        let symbol = textSymbol
        return symbol.replacingOccurrences(of: "#", with: "Sharp").replacingOccurrences(of: "b", with: "Flat")
    }
}

extension ChartView: PageAware {
    
    var pageCount: Int {
        return pages.count
    }
    
    func frame(forPageAt index: Int) -> NSRect {
        return rectForPage(index + 1)
    }
}

class ChartDocumentExporter: NSObject {
    
    lazy var exporterAccessoryView: NSView? = {
        var topObjects: NSArray?
        NSNib(nibNamed: .exporterAccessoryView, bundle: nil)?.instantiate(withOwner: self, topLevelObjects: &topObjects)
        for object in topObjects! {
            switch object {
            case let view as NSView:
                return view
            default:
                break
            }
        }
        return nil
    }()
    
    /// The keys to export PDFs in.
    var keysToExport: Set<MusicalNote> = []
    
    @IBAction func keyCheckboxChecked(_ sender: NSButton) {
        guard let key = MusicalNote(textSymbol: sender.title) else { return }
        if sender.state == .on {
            keysToExport.insert(key)
        } else {
            keysToExport.remove(key)
        }
    }
    
    func configureExportPanel() -> NSOpenPanel {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = true
        panel.canChooseFiles = false
        panel.canCreateDirectories = true
        panel.prompt = "Export"
        
        panel.accessoryView = exporterAccessoryView
        panel.isAccessoryViewDisclosed = true
        return panel
    }
    
    func export(_ document: ChartDocument) {
        let panel = configureExportPanel()
        
        let song = document.song
        let baseFileName = baseName(for: document)
        panel.beginSheetModal(for: document.chartWindowController.window!) { (response) in
            guard let dir = panel.directoryURL, response == NSApplication.ModalResponse.OK else { return }
            
            self.exportInChosenKeys(song, named: baseFileName, to: dir)
        }
    }
    
    func baseName(for document: ChartDocument) -> String {
        if let currentFilename = document.fileURL?.deletingPathExtension().lastPathComponent {
            return currentFilename
        } else {
            let songTitle = document.song.meta.title
            
            return songTitle.urlSafeVersion() ?? "Untitled"
        }
    }
    
    func createPrintableChartView(for song: Song) -> ChartView {
        let printableView = ChartView(frame: NSRect.zero)
        
        // Use the mapped array as the section source. This is only possible because Arrays of SectionViews conform to the SectionSource protocol.
        let itemSource = song.sequence.map { section in
            return SectionView(representedSection: section)
        }
        printableView.itemSource = itemSource
        
        let pageSource = ChartPageSource(songMeta: song.meta)
        printableView.pageSource = pageSource
        
        // Layout items and pages
        printableView.reloadItems()
        
        // Now layout the views that represent the layout.
        printableView.needsLayout = true
        printableView.layoutSubtreeIfNeeded()
        
        return printableView
    }
    
    /// The filename will look like: baseNameHere-keyURLSafeNameHere.pdf
    func formURL(inDirectory directory: URL, forBaseName baseName: String, key: MusicalNote) -> URL {
        var url = directory.appendingPathComponent(baseName+"-\(key.urlSafeSymbol)")
        url.appendPathExtension("pdf")
        return url
    }
    
    func exportInChosenKeys(_ song: Song, named baseName: String, to directory: URL) {
        for key in self.keysToExport {
            self.exportInOneKey(song.transposed(to: key), named: baseName, to: directory)
        }
    }
    
    func exportInOneKey(_ song: Song, named baseName: String, to directory: URL) {
        let printingView: ChartView = createPrintableChartView(for: song)
        
        let pdfDocument: PDFDocument = PDFDocument(convertibleView: printingView)
        
        let url = formURL(inDirectory: directory, forBaseName: baseName, key: song.meta.key)
        pdfDocument.write(to: url)
    }
}
