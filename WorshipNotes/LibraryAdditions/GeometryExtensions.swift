//
//  GeometryExtensions.swift
//  DocEx
//
//  Created by Joseph Van Boxtel on 1/26/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSRect {
    
    /// Each edge of the rectangle is pushed out by the corrosponding value on the passed in insets.
    func outset(by insets: NSEdgeInsets) -> NSRect {
        let outset = NSRect(x: self.origin.x - insets.left, y: self.origin.y - insets.bottom, width: self.width + insets.right + insets.left, height: self.height + insets.top + insets.bottom)
        return outset
    }
    
    /// Each edge of the rectangle is pull in by the corrosponding value on the passed in insets.
    func inset(by insets: NSEdgeInsets) -> CGRect {
        return CGRect(x: self.minX + insets.left, y: self.minY + insets.bottom,
                      width: self.width - insets.left - insets.right,
                      height: self.height - insets.top - insets.bottom)
    }
}

extension NSRect {
    
    /// The center of the rectangle.
    var center: NSPoint {
        return NSPoint(x: origin.x + width/2, y: origin.y + height/2)
    }
}


extension NSEdgeInsets {
    
    /// Switch the top and bottom insets. Useful when dealing with Views using flipped coordinate spaces.
    func flippedVertically() -> NSEdgeInsets {
        var flipped = NSEdgeInsets()
        flipped.left = left
        flipped.right = right
        
        // Switch top and bottom insets
        flipped.top = bottom
        flipped.bottom = top
        return flipped
    }
    
    /// Scale all four insets by the given factor.
    func scaled(by factor: CGFloat) -> NSEdgeInsets {
        var scaled = NSEdgeInsets()
        scaled.left = left * factor
        scaled.right = right * factor
        scaled.top = top * factor
        scaled.bottom = bottom * factor
        return scaled
    }
}
