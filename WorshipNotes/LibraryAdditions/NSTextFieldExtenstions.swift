//
//  NSTextFieldExtenstions.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 7/19/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSTextField {
    func setStringValueHidingIfNil(_ string: String?) {
        if let string = string {
            stringValue = string
            isHidden = false
        } else {
            isHidden = true
        }
    }
}
