//
//  StringExtensions.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 8/2/18.
//  Copyright © 2018 Joseph Van Boxtel. All rights reserved.
//

import Foundation

extension String {
    
    /// Returns a version of the reciever whose first character is now uppercase.
    func uppercaseFirst() -> String {
        return self[...startIndex].uppercased() + self[index(after: startIndex)...]
    }
    
    /// Transforms the string to latin, removes all non alphanumerics, and uses UpperCamelCase using the non alphanumberics as word boundaries.
    func urlSafeVersion() -> String? {
        if let latin = self.applyingTransform(StringTransform.toLatin, reverse: false) {
            let urlComponents = latin.components(separatedBy: CharacterSet.alphanumerics.inverted)
            let result = urlComponents.filter { $0 != "" }.map { $0.lowercased().uppercaseFirst() }.joined(separator: "")
            
            if result.count > 0 {
                return result
            }
        }
        
        return nil
    }
}
