//
//  ConstraintHelpers.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 3/9/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Cocoa

extension NSEdgeInsets {
    static let zero: NSEdgeInsets = NSEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
}

extension NSView {
    
    func addConstraintToSuperview(_ constraint: NSLayoutConstraint) {
        guard let superview = superview else {
            fatalError("Adding constraint to a superview that doesn't exist")
        }
        superview.addConstraint(constraint)
    }
    
    @discardableResult
    func constrain(_ ownAttribute: NSLayoutConstraint.Attribute, to view: NSView, attribute otherAttribute: NSLayoutConstraint.Attribute, constant: CGFloat = 0, multiplier: CGFloat = 1) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: ownAttribute, relatedBy: .equal, toItem: view, attribute: otherAttribute, multiplier: multiplier, constant: constant)
        addConstraintToSuperview(constraint)
        return constraint
    }
    
    @discardableResult
    func constrain(_ attribute: NSLayoutConstraint.Attribute, to view: NSView, constant: CGFloat = 0, multiplier: CGFloat = 1) -> NSLayoutConstraint {
        return constrain(attribute, to: view, attribute: attribute, constant: constant, multiplier: multiplier)
    }
    
    @discardableResult
    func constrainToEdges(of view: NSView, _ insets: NSEdgeInsets = .zero) -> [NSLayoutConstraint] {
        return [
            constrain(.left, to: view, constant: insets.left),
            constrain(.right, to: view, constant: -insets.right),
            constrain(.top, to: view, constant: -insets.top),
            constrain(.bottom, to: view, constant: insets.bottom)
        ]
    }
    
    /// Constrains the width to the height with the given ratio. width/height
    @discardableResult
    func constrainAspectRatio(_ ratio: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: ratio, constant: 0)
        addConstraint(constraint)
        return constraint
    }
    
    @discardableResult
    func constrain(_ attribute: NSLayoutConstraint.Attribute, to value: CGFloat)  -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: value)
        addConstraint(constraint)
        return constraint
    }
}
