//
//  CollectionExtensions.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 7/30/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import Foundation

extension Collection {
    func isIndexValid(_ i: Index) -> Bool {
        return indices.contains(i)
    }
}
