//
//  GraphicsExtensions.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/28/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

extension CALayer {

    /// Get `NSImage` representation of the layer.
    func renderToImage() -> NSImage {
        let width = Int(bounds.width * self.contentsScale)
        let height = Int(bounds.height * self.contentsScale)
        let imageRepresentation = NSBitmapImageRep(bitmapDataPlanes: nil, pixelsWide: width, pixelsHigh: height, bitsPerSample: 8, samplesPerPixel: 4, hasAlpha: true, isPlanar: false, colorSpaceName: NSColorSpaceName.deviceRGB, bytesPerRow: 0, bitsPerPixel: 0)!
        imageRepresentation.size = bounds.size
        
        let context = NSGraphicsContext(bitmapImageRep: imageRepresentation)!
        
        render(in: context.cgContext)
        
        return NSImage(cgImage: imageRepresentation.cgImage!, size: bounds.size)
    }
}

extension CAShapeLayer {
    
    public convenience init(circleOfRadius radius: CGFloat) {
        self.init()
        path = CGPath(ellipseIn: CGRect(x: 0, y: 0, width: radius*2, height: radius*2), transform: nil)
        frame = CGRect(x: 0, y: 0, width: 2*radius, height: 2*radius)
    }
}

extension NSView {
    
    func addBoundingBox(constrainedTo view: NSView, fillColor: NSColor) {
        let box = NSBox(frame: view.frame)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.fillColor = fillColor
        box.borderType = .noBorder
        box.boxType = .custom
        addSubview(box)
        box.constrainToEdges(of: view)
    }
}

extension NSColor {
    var transparent: NSColor? {
        return self.blended(withFraction: 0.5, of: .clear)!
    }
    
    var wantsLightFont: Bool {
        let rgb = self.usingColorSpace(.genericRGB)!
        let perceptiveBrightness = 1 - ( 0.299 * rgb.redComponent + 0.587 * rgb.greenComponent + 0.114 * rgb.blueComponent)
        return perceptiveBrightness >= 0.5
    }
    
    static func randomColor() -> NSColor {
        return NSColor(calibratedRed: CGFloat(arc4random_uniform(255))/255, green: CGFloat(arc4random_uniform(255))/255, blue: CGFloat(arc4random_uniform(255))/255, alpha: 1.0)
    }
}
