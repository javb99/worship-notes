//
//  PDFExtensions.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 8/2/18.
//  Copyright © 2018 Joseph Van Boxtel. All rights reserved.
//

import Quartz

/// Adopters are aware of how many pages to use to render their content and where each page goes.
public protocol PageAware {
    
    /// The number of pages to use when creating a PDFDocument or other printing related task for this view.
    var pageCount: Int { get }
    
    /// The rectangle that fully encompasses the page at the given index.
    ///
    /// - Parameter index: 0-based index for the page.
    /// - Returns: A NSRect in view coordinates.
    func frame(forPageAt index: Int) -> NSRect
}

public extension PDFDocument {
    
    public typealias PDFConvertibleView = PageAware & NSView
    
    /// Creats a PDFDocument by rendering each page frame into an image from PDF data provided by NSView's dataWithPDF(inside:) method and adding it as a PDFPage.
    ///
    /// - Parameter convertibleView: The view to render to create the PDFDocument.
    public convenience init(convertibleView: PDFConvertibleView) {
        self.init()
        
        for i in 0..<convertibleView.pageCount {
            let pageFrame = convertibleView.frame(forPageAt: i)
            let data = convertibleView.dataWithPDF(inside: pageFrame)
            let pdfPage = PDFPage(image: NSImage(data: data)!)!
            insert(pdfPage, at: i)
        }
    }
}
