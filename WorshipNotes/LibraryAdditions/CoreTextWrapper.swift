//
//  CoreTextWrapper.swift
//  WorshipNotes
//
//  Created by Joseph Van Boxtel on 11/26/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import Cocoa

/// A wrapper for CTFramesetter
class Framesetter {
    
    var ctFramesetter: CTFramesetter
    var attributedString: NSAttributedString
    
    init(attributedString: NSAttributedString) {
        self.attributedString = attributedString
        ctFramesetter = CTFramesetterCreateWithAttributedString(attributedString as CFAttributedString)
    }
    
    /// Determines the frame size needed for a string range.
    /// This function can be used to determine how much space is needed to display a string, optionally by constraining the space along either dimension.
    ///
    /// - Parameters:
    ///   - textRange: The string range to which the frame size applies. The string range is a range over the string used to create the framesetter. If the length portion of the range is set to 0, then the framesetter continues to add lines until it runs out of text or space.
    ///   - maxSize: The width and height to which the frame size is constrained. A value of CGFLOAT_MAX for either dimension indicates that it should be treated as unconstrained.
    ///   - frameAttributes: Additional attributes that control the frame filling process, or nil if there are no such attributes.
    /// - Returns: suggestion is the actual dimensions for the given string range and constraints.
    /// actuallyFit is the range of characters that actually fit.
    func suggestedFrameSize(textRange: NSRange, maxSize: CGSize, frameAttributes: [String: Any]?) -> (suggestion: CGSize, actuallyFit: NSRange) {
        
        var actuallyFit: CFRange = CFRange(location: 0, length: 0)
        let cRange = CFRangeMake(textRange.location, textRange.length)
        
        let suggested = CTFramesetterSuggestFrameSizeWithConstraints(ctFramesetter, cRange, frameAttributes as CFDictionary?, maxSize, &actuallyFit)
        
        return (suggestion: suggested, actuallyFit: NSRange(location: actuallyFit.location, length: actuallyFit.length))
    }
}

/// A wrapper for CTFrame
class TextFrame {
    /// The backing CTFrame of this object.
    var ctFrame: CTFrame
    
    /// Creates a Swift wrapper to the Core Text class CTFrame
    ///
    /// - Parameters:
    ///   - framesetter: The framesetter used to create the frame.
    ///   - textRange: The range of characters in the string to use in the frame. A count of zero indicates unlimited length - Use the whole string.
    ///   - path: Specifies the shape the frame will take on.
    ///   - attributes: Additional attributes to control the frame filling process.
    init(framesetter: Framesetter, textRange: NSRange, boundingPath path: CGPath, attributes: [String: Any]?) {
        let ctFramesetter = framesetter.ctFramesetter
        let cRange = CFRangeMake(textRange.location, textRange.length)
        let cAtts = attributes as CFDictionary?
        ctFrame = CTFramesetterCreateFrame(ctFramesetter, cRange, path, cAtts)
    }
    
    func draw(in ctx: CGContext) {
        CTFrameDraw(ctFrame, ctx)
    }
    
    // Might need to be lazy.
    var lines: [TextLine] {
        let cLines = CTFrameGetLines(ctFrame) as [AnyObject]
        let lines = cLines.map { TextLine($0 as! CTLine) }
        return lines
    }
    
    /// A wrapper for CTFrameGetLineOrigins
    ///
    /// - Parameter stringRange: The range of characters to use lines from. Don't use length of zero.
    /// - Returns: An array of CGPoint each point is the origin of a line. The origin is relative to the origin of the frame in the bottom left.
    func lineOriginPoints(forStringRange stringRange: NSRange) -> [CGPoint] {
        let cRange = CFRangeMake(stringRange.location, stringRange.length)
        var origins: [CGPoint] = Array<CGPoint>(repeating: CGPoint.zero, count: stringRange.length)
        CTFrameGetLineOrigins(ctFrame, cRange, &origins)
        return origins
    }
}

/// A wrapper for CTLine
class TextLine {
    var ctLine: CTLine
    
    init(_ ctLine: CTLine) {
        self.ctLine = ctLine
    }
    
    /// The range that this line occupies.
    var textRange: NSRange {
        let cRange = CTLineGetStringRange(ctLine)
        return NSRange(location: cRange.location, length: cRange.length)
    }
    
    /// The offset along the baseline of the character.
    ///
    /// - Parameter stringIndex: The index of the character.
    /// - Returns: offset from the origin along the baseline.
    func lineOffset(forStringIndex stringIndex: Int) -> CGFloat {
        return CTLineGetOffsetForStringIndex(ctLine, stringIndex as CFIndex, nil)
    }
}
