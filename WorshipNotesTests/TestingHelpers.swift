//
//  TestingHelpers.swift
//  WorshipNotesTests
//
//  Created by Joseph Van Boxtel on 8/3/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func measure(afterCleaningDirectoryAt directory: URL, block: ()->Void) {
        let fileManager = FileManager.default
        self.measureMetrics([.wallClockTime], automaticallyStartMeasuring: false) {
            
            for item in try! fileManager.contentsOfDirectory(atPath: directory.path).map{ directory.appendingPathComponent($0) } {
                try! fileManager.removeItem(at: item)
            }
            //try! fileManager.createDirectory(at: directory, withIntermediateDirectories: false, attributes: nil)
            startMeasuring()
            block()
            stopMeasuring()
        }
    }
}
