//
//  TranspositionTests.swift
//  WorshipNotesTests
//
//  Created by Joseph Van Boxtel on 12/8/17.
//  Copyright © 2017 John Van Boxtel. All rights reserved.
//

import XCTest
@testable import WorshipNotes

class TranspositionTests: XCTestCase {

    var song: Song!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let meta: SongMeta = SongMeta(title: "Fierce", authors: "Authors", key: .g, tempo: 146, measure: "4/4", copyright: "")
        song = Song(meta: meta, sequence: [])
        var verse = Section(abriviation: "V1", title: "Verse 1")
        verse.rawText = """
        Verse 1
        Before I [C2]call before I ever [G]cry
        You answer [Em]me from where the thunder [Dsus]hides
        I can't out - [C2]run this heart I'm tethered [G]to
        With ev'ry [Em]step I collide with [Dsus/F#]You
        """
        song.sequence.append(verse)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    let a = MusicalNote.a
    let bFlat = MusicalNote.b.flat()
    let c = MusicalNote.c
    let f = MusicalNote.f
    let g = MusicalNote.g
    
    func testLetterDistance() {

        XCTAssertEqual(a.baseLetter.distance(to: bFlat.baseLetter), 1)
        XCTAssertEqual(bFlat.baseLetter.distance(to: a.baseLetter), -1)
        
        // Over the wrap boundary
        XCTAssertEqual(bFlat.baseLetter.distance(to: c.baseLetter), 1)
        XCTAssertEqual(c.baseLetter.distance(to: bFlat.baseLetter), -1)
        
        XCTAssertEqual(f.baseLetter.distance(to: g.baseLetter), 1)
        XCTAssertEqual(g.baseLetter.distance(to: f.baseLetter), -1)
    }

    func testSemitoneDistance() {
        let a = MusicalNote.a
        let bFlat = MusicalNote.b.flat()
        let c = MusicalNote.c
        let f = MusicalNote.f
        let g = MusicalNote.g
        
        XCTAssertEqual(a.semitones(to: bFlat), 1)
        XCTAssertEqual(bFlat.semitones(to: a), -1)
        
        // Over the wrap boundary
        XCTAssertEqual(bFlat.semitones(to: c), 2)
        XCTAssertEqual(c.semitones(to: bFlat), -2)
        
        XCTAssertEqual(f.semitones(to: g), 2)
        XCTAssertEqual(g.semitones(to: f), -2)
    }
    
    func testTransposeSmallUp() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let oldKey = MusicalNote.g
        let newKey = MusicalNote.a
        
        // 1
        let one = MusicalNote.g
        let oneTransposed = one.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(oneTransposed, MusicalNote.a)
        
        // 4
        let four = MusicalNote.c
        let fourTransposed = four.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(fourTransposed, MusicalNote.d)
        
        // 7
        let seven = MusicalNote.f.sharp()
        let sevenTransposed = seven.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(sevenTransposed, MusicalNote.g.sharp())
    }
    
    func testTransposeLargeUp() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let oldKey = MusicalNote.g
        let newKey = MusicalNote.f
        
        // 1
        let one = MusicalNote.g
        let oneTransposed = one.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(oneTransposed, MusicalNote.f)
        
        // 4
        let four = MusicalNote.c
        let fourTransposed = four.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(fourTransposed, MusicalNote.b.flat())
        
        // 7
        let seven = MusicalNote.f.sharp()
        let sevenTransposed = seven.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(sevenTransposed, MusicalNote.e)
    }
    
    func testTransposeSmallDown() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let oldKey = MusicalNote.a
        let newKey = MusicalNote.g
        
        // 1
        let one = MusicalNote.a
        let oneTransposed = one.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(oneTransposed, MusicalNote.g)
        
        // 4
        let four = MusicalNote.d
        let fourTransposed = four.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(fourTransposed, MusicalNote.c)
        
        // 7
        let seven = MusicalNote.g.sharp()
        let sevenTransposed = seven.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(sevenTransposed, MusicalNote.f.sharp())
    }
    
    func testTransposeLargeDown() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let oldKey = MusicalNote.f
        let newKey = MusicalNote.g
        
        // 1
        let one = MusicalNote.f
        let oneTransposed = one.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(oneTransposed, MusicalNote.g)
        
        // 4
        let four = MusicalNote.b.flat()
        let fourTransposed = four.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(fourTransposed, MusicalNote.c)
        
        // 7
        let seven = MusicalNote.e
        let sevenTransposed = seven.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(sevenTransposed, MusicalNote.f.sharp())
    }
    
    func testTransposeOutOfKey() {
        let oldKey = MusicalNote.g
        let newKey = MusicalNote.a
        
        // Flat 7
        let flatSeven = MusicalNote.f
        let flatSevenTransposed = flatSeven.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(flatSevenTransposed, MusicalNote.g)
    }
    
    func testIssueWithFlats() {
        let oldKey = MusicalNote.b.flat()
        let newKey = MusicalNote.e
        
        let four = MusicalNote.e.flat()
        let fourTransposed = four.transposed(from: oldKey, to: newKey)
        XCTAssertEqual(fourTransposed, MusicalNote.a)
    }
    
    func testTransposeChordPhrase() {
        let phraseDiffRoot = ChordPhrase(index: 10, text: "D/F#")
        XCTAssertEqual(phraseDiffRoot.transposed(from: .g, to: .c).text, "G/B")
        
        let phraseMinor = ChordPhrase(index: 10, text: "Em")
        XCTAssertEqual(phraseMinor.transposed(from: .g, to: .e).text, "C#m")
        
        let phraseTwoChords = ChordPhrase(index: 10, text: "Em  |  G  /  Am")
        XCTAssertEqual(phraseTwoChords.transposed(from: .g, to: .e).text, "C#m  |  E  /  F#m")
    }
    
    func testTransposeSection() {
        let expectedRaw = """
        [A]Here is all [E/G#]my [B]love, it’s Yours
        [C#m]No con[E/G#]ditions
        [A]When You pull [E/G#]me [B]close
        No I [C#m]won’t re[E]sist it
        """
        var section = Section(abriviation: "C", title: "Chorus")
        section.rawText = """
        [F]Here is all [C/E]my [G]love, it’s Yours
        [Am]No con[C/E]ditions
        [F]When You pull [C/E]me [G]close
        No I [Am]won’t re[C]sist it
        """
        
        let transposed = section.transposed(from: .c, to: .e)
        XCTAssertEqual(transposed.rawText, expectedRaw)
    }
}
