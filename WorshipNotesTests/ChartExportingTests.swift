//
//  ChartExportingTests.swift
//  WorshipNotesTests
//
//  Created by Joseph Van Boxtel on 8/2/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import XCTest
@testable import WorshipNotes

class ChartExportingTests: XCTestCase {
    
    var exporter: ChartDocumentExporter!
    
    let testsDir = URL(fileURLWithPath: "/Users/josephvanboxtel/ProgrammingOutput/WorshipNotes/Tests")
    
    let song: Song = {
        let meta: SongMeta = SongMeta(title: "Fierce", authors: "Authors", key: .g, tempo: 146, measure: "4/4", copyright: "")
        var song = Song(meta: meta, sequence: [])
        var verse = Section(abriviation: "V1", title: "Verse 1")
        verse.rawText = """
        Verse 1
        Before I [C2]call before I ever [G]cry
        You answer [Em]me from where the thunder [Dsus]hides
        I can't out - [C2]run this heart I'm tethered [G]to
        With ev'ry [Em]step I collide with [Dsus/F#]You
        """
        for _ in 0..<100 {
            song.sequence.append(verse)
        }
        return song
    }()
    
    var tookOurPlaceDocument: ChartDocument!
    var untitledDocument: ChartDocument!

    override func setUp() {
        let bundle = Bundle(identifier: "johnvb.WorshipNotesTests")!
        
        let tookOurPlaceURL = bundle.url(forResource: "TookOurPlace", withExtension: "wcc")!
        tookOurPlaceDocument = try! ChartDocument(contentsOf: tookOurPlaceURL, ofType: "wcc")
        
        untitledDocument = ChartDocument()
        for section in song.sequence {
            untitledDocument.append(section)
        }
        
        //try! untitledDocument.data(ofType: "wcc").write(to: testsDir.appendingPathComponent("onehundredVerses.wcc"))
        
        exporter = ChartDocumentExporter()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExporterAccessoryView() {
        let accView = exporter.exporterAccessoryView!
        let horizStack = accView.subviews.first!
        let vertStacks = horizStack.subviews
        XCTAssertEqual(vertStacks[0].subviews.map { ($0 as! NSButton).title }, ["Ab", "A", "A#"])
        XCTAssertEqual(vertStacks[1].subviews.map { ($0 as! NSButton).title }, ["Bb", "B", "C"])
        XCTAssertEqual(vertStacks[4].subviews.map { ($0 as! NSButton).title }, ["F", "F#", "Gb"])
    }
    
    func testKeyCheckboxChecked() {
        let button = NSButton(frame: NSRect.zero)
        button.state = .on
        
        let titles = ["Ab", "A", "A#", "Bb", "B", "C", "C#", "Db", "D", "D#", "Eb", "E", "F", "F#", "Gb", "G", "G#"]
        /// Check all boxes.
        for title in titles {
            button.title = title
            exporter.keyCheckboxChecked(button)
        }
        XCTAssertEqual(exporter.keysToExport.count, titles.count)
        
        // Uncheck a few.
        button.state = .off
        
        button.title = "A#"
        exporter.keyCheckboxChecked(button)
        XCTAssertFalse(exporter.keysToExport.contains(MusicalNote.a.sharp()))
        
        button.title = "Bb"
        exporter.keyCheckboxChecked(button)
        XCTAssertFalse(exporter.keysToExport.contains(MusicalNote.b.flat()))

        button.title = "G#"
        exporter.keyCheckboxChecked(button)
        XCTAssertFalse(exporter.keysToExport.contains(MusicalNote.g.sharp()))
        
        XCTAssertEqual(exporter.keysToExport.count, titles.count - 3)
    }
    
    func testURLSafeString() {
        XCTAssertEqual("Hello World!!!!".urlSafeVersion()!, "HelloWorld")
        XCTAssertEqual("Hello this is a great song".urlSafeVersion()!, "HelloThisIsAGreatSong")
        XCTAssertEqual("Billy's greAtest Hit YET...".urlSafeVersion()!, "BillySGreatestHitYet")
        XCTAssertEqual("Hello W😍rld!!!!".urlSafeVersion()!, "HelloWRld")
        XCTAssertEqual("Hello W/rld!!!!".urlSafeVersion()!, "HelloWRld")
        XCTAssertEqual("Hello World!!!!".urlSafeVersion()!, "HelloWorld")
        XCTAssertEqual("Hello World!!!!".urlSafeVersion()!, "HelloWorld")
    }

    func testBaseName() {
        let baseNameA = tookOurPlaceDocument.exporter.baseName(for: tookOurPlaceDocument)
        XCTAssertEqual(baseNameA, "TookOurPlace")
        
        let baseNameB = untitledDocument.exporter.baseName(for: untitledDocument)
        XCTAssertEqual(baseNameB, "Untitled")
    }
    
    func testFormURL() {
        XCTAssertEqual(exporter.formURL(inDirectory: testsDir, forBaseName: "SongA", key: .c).absoluteString, testsDir.absoluteString + "SongA-C.pdf")
        XCTAssertEqual(exporter.formURL(inDirectory: testsDir, forBaseName: "SongB", key: MusicalNote.c.sharp()).absoluteString, testsDir.absoluteString + "SongB-CSharp.pdf")
    }
    
    func testCreatePrintableChartView() {
        let chart = ChartView(frame: NSRect.zero)
        exporter.configure(chartView: chart, for: song)
        XCTAssertEqual(chart.itemSource!.numberOfItemViews(), 100)
        XCTAssertEqual(chart.pageCount, 17)
        XCTAssertGreaterThan(chart.frame.size.height, 0)
        XCTAssertGreaterThan(chart.frame.size.width, 0)
    }
    
    /// Render a chart with 2 pages.
    func testSmallSingleKeyRenderingPerformance() {
        // This is an example of a performance test case.
        let directory = testsDir.appendingPathComponent("/ExportingTests/Performance")
        measure(afterCleaningDirectoryAt: directory) {
            tookOurPlaceDocument.exporter.exportInOneKey(tookOurPlaceDocument.song, named: "PerformanceTestSmall", to: directory)
        }
    }

    /// Render a chart with 100 verses(17 pages).
    func testLargeSingleKeyRenderingPerformance() {
        // This is an example of a performance test case.
        let directory = testsDir.appendingPathComponent("/ExportingTests/Performance")
        measure(afterCleaningDirectoryAt: directory) {
            exporter.exportInOneKey(song, named: "PerformanceTestLarge", to: directory)
        }
    }
    
    /// Render a chart in 7 different keys with 100 verses(17 pages).
    func testLarge7KeysRenderingPerformance() {
        exporter.keysToExport = [.a, .b, .c, .d, .e, .f, .g]
        let directory = testsDir.appendingPathComponent("/ExportingTests/Performance")
        
        measure(afterCleaningDirectoryAt: directory) {
            exporter.exportInChosenKeys(song, named: "PerformanceTestLarge", to: directory)
        }
    }
}

