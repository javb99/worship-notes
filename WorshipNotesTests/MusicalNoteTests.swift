//
//  MusicalNoteTests.swift
//  WorshipNotesTests
//
//  Created by Joseph Van Boxtel on 8/3/18.
//  Copyright © 2018 John Van Boxtel. All rights reserved.
//

import XCTest
@testable import WorshipNotes

class MusicalNoteTests: XCTestCase {
    
    let allNotes: [MusicalNote] = [.a, MusicalNote.a.flat(), MusicalNote.a.sharp(), .b, MusicalNote.b.flat(), .c, MusicalNote.c.sharp(), .d, MusicalNote.d.flat(), MusicalNote.d.sharp(), .e, MusicalNote.e.flat(), .f, MusicalNote.f.sharp(), .g, MusicalNote.g.flat(), MusicalNote.g.sharp()]
    let allNotesNoFlats: [MusicalNote] = [.a, MusicalNote.a.sharp(), .b, .c, MusicalNote.c.sharp(), .d, MusicalNote.d.sharp(), .e, .f, MusicalNote.f.sharp(), .g, MusicalNote.g.sharp()]

    func testHash() {
        let hashes: [Int] = allNotes.map{ $0.hashValue }
        let setOfHashes = Set(hashes)
        /// The hashing algorithm should generate unique values for all the notes.
        XCTAssertEqual(hashes.count, setOfHashes.count)
    }
    
    func testEquatable() {
        XCTAssertEqual(MusicalNote.c, MusicalNote.c)
        XCTAssertEqual(MusicalNote.c.sharp(), MusicalNote.c.sharp())
        XCTAssertNotEqual(MusicalNote.c.sharp(), MusicalNote.d.flat())
    }
    
    func testTextSymbol() {
        XCTAssertEqual(MusicalNote.c.textSymbol, "C")
        XCTAssertEqual(MusicalNote.a.sharp().textSymbol, "A#")
        XCTAssertEqual(MusicalNote.g.sharp().sharp().textSymbol, "G##")
        XCTAssertEqual(MusicalNote.f.flat().textSymbol, "Fb")
        XCTAssertEqual(MusicalNote.e.flat().flat().textSymbol, "Ebb")
    }
    
    func testInitTextSymbol() {
        XCTAssertEqual(MusicalNote(textSymbol: "C"), MusicalNote.c)
        XCTAssertEqual(MusicalNote(textSymbol: "A#"), MusicalNote.a.sharp())
        XCTAssertEqual(MusicalNote(textSymbol: "Bb"), MusicalNote.b.flat())
        XCTAssertEqual(MusicalNote(textSymbol: "C #"), nil)
        XCTAssertEqual(MusicalNote(textSymbol: "c"), nil)
        XCTAssertEqual(MusicalNote(textSymbol: "CB"), nil)
        XCTAssertEqual(MusicalNote(textSymbol: "C#F"), nil)
        XCTAssertEqual(MusicalNote(textSymbol: "C/B"), nil)
    }
    
    func testInitBaseLetter() {
        XCTAssertEqual(MusicalNote(baseLetter: .a), MusicalNote.a)
        XCTAssertEqual(MusicalNote(baseLetter: .g), MusicalNote.g)
        XCTAssertEqual(MusicalNote(baseLetter: .c), MusicalNote.c)
        XCTAssertEqual(MusicalNote(semitones: 2, baseLetter: .c), MusicalNote.c.sharp().sharp())
        XCTAssertEqual(MusicalNote(semitones: -2, baseLetter: .c), MusicalNote.c.flat().flat())
        XCTAssertEqual(MusicalNote(semitones: 4, baseLetter: .e), MusicalNote.e)
        XCTAssertEqual(MusicalNote(semitones: 5, baseLetter: .e), MusicalNote.e.sharp())
    }
    
    func testSharp() {
        XCTAssertEqual(MusicalNote.c.sharp().textSymbol, "C#")
        XCTAssertEqual(MusicalNote.a.sharp().textSymbol, "A#")
        XCTAssertEqual(MusicalNote.g.sharp().textSymbol, "G#")
        XCTAssertEqual(MusicalNote.b.sharp().textSymbol, "B#")
    }
    
    func testFlat() {
        XCTAssertEqual(MusicalNote.c.flat().textSymbol, "Cb")
        XCTAssertEqual(MusicalNote.a.flat().textSymbol, "Ab")
        XCTAssertEqual(MusicalNote.g.flat().textSymbol, "Gb")
        XCTAssertEqual(MusicalNote.b.flat().textSymbol, "Bb")
    }
    
    func testSharpenAndFlatten() {
        var note = MusicalNote.a
        note.sharpen()
        XCTAssertEqual(note.textSymbol, "A#")
        
        note.flatten()
        XCTAssertEqual(note.textSymbol, "A")
        
        note = MusicalNote.a
        note.sharpen()
        note.flatten()
        note.flatten()
        XCTAssertEqual(note.textSymbol, "Ab")
    }

}
