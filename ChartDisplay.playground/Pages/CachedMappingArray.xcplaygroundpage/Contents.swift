//: [Previous](@previous)

import Foundation

public class CachedMappingArray<RawElement, MappedElement> {
    
    private var backingArray: Array<RawElement> = []
    
    private var cache: [Index : MappedElement] = [:]
    
    let transform: (RawElement)->MappedElement
    
    init(transform: @escaping (RawElement)->MappedElement) {
        self.transform = transform
    }
    
    init<S: Sequence>(_ rawElements: S, transform: @escaping (RawElement)->MappedElement) where S.Element == RawElement {
        self.transform = transform
        backingArray = Array<RawElement>(rawElements)
    }
    
    func isIndexValid(_ i: Index) -> Bool {
        return i <= endIndex && i >= startIndex
    }
    
    /// Checks wether i is valid. Fails a precondition if not.
    func checkIndex(_ i: Index) {
        precondition(isIndexValid(i), "Index out of bounds")
    }
    
    public func setElement(at i: Index, to element: RawElement) {
        checkIndex(i)
        backingArray[i] = element
        clearCachedElement(at: i)
    }
    
    public func moveElement(at fromIndex: Index, to toIndex: Index) {
        checkIndex(fromIndex)
        checkIndex(toIndex)
        
        let element = backingArray.remove(at: fromIndex)
        backingArray.insert(element, at: toIndex)
        clearCachedElements(startingAt: Swift.min(fromIndex, toIndex))
    }
    
    // Doesn't return the element because it isn't obvious wether it would return the mapped or unmapped version. Returning the unmapped element doesn't seem consistent with getting the mapped version from the subscrip getter.
    public func removeElement(at i: Index) {
        checkIndex(i)
        backingArray.remove(at: i)
        clearCachedElements(startingAt: i)
    }
    
    public func insert(_ element: RawElement, at i: Index) {
        checkIndex(i)
        backingArray.insert(element, at: i)
        clearCachedElements(startingAt: i)
    }
    
    public func clearCachedElement(at i: Index) {
        checkIndex(i)
        cache[i] = nil
    }
    
    public func clearCachedElements(in range: Range<Index>) {
        assert(!range.isEmpty && isIndexValid(range.first!) && isIndexValid(range.last!), "Range is out of bounds or empty")
        for i in range {
            clearCachedElement(at: i)
        }
    }
    
    public func clearCachedElements(startingAt i: Index) {
        guard !cache.isEmpty else { return }
        assert(isIndexValid(i), "Index out of bounds")
        clearCachedElements(in: i..<backingArray.endIndex)
    }
}

/// Couldn't conform to Collection because the subscript getter is mutating.
extension CachedMappingArray: Collection {
    
    public typealias Element = MappedElement
    public typealias Index = Int
    
    public var startIndex: Index {
        return backingArray.startIndex
    }
    
    public var endIndex: Index {
        return backingArray.endIndex
    }
    
    public func index(after i: Int) -> Int {
        return backingArray.index(after: i)
    }
    
    public subscript(position: Index) -> MappedElement {
        get {
            assert(isIndexValid(position), "Index out of bounds")
            if let mappedElement = cache[position] {
                // Cache hit.
                // Return the cached item.
                return mappedElement
                
            } else {
                // Cache missed.
                // Transform the element at the given index, store it in the cache, and return it.
                let mappedElement = transform(backingArray[position])
                cache[position] = mappedElement // O(1) complexity because hashing Ints works well.
                print("Cache missed for index: \(position)")
                return mappedElement
            }
        }
    }
}

let data = [1, 2, 3, 4]
let cache = CachedMappingArray(data) { int in
    return "\(int)"
}
for string in cache {
    print(string)
}

//: [Next](@next)
