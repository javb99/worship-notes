import Foundation

/// Represents a chord in a string that has been parsed.
public struct Chord {
    
    public init(text: NSAttributedString, range: NSRange) {
        self.range = range
        self.text = text
    }
    
    /// The text for the chord, excluding the brackets.
    public var text: NSAttributedString
    
    /// The character range this chord occupies in the parsed string.
    public var range: NSRange
    
    public var textExcludingBrackets: NSAttributedString {
        let excludingBrackets = NSRange(location: 1, length: range.length-2)
        return text.attributedSubstring(from: excludingBrackets)
    }
    
    /// The character range that this chord occupies excluding the brackets('[' and ']').
    public var rangeExcludingBrackets: NSRange {
        return NSRange(location: range.location-1, length: range.length-2)
    }
}

extension Chord: Equatable {
    public static func ==(lhs: Chord, rhs: Chord) -> Bool {
        return lhs.range == rhs.range
    }
}

extension Chord: CustomPlaygroundDisplayConvertible {
    public var playgroundDescription: Any {
        return "\(text.string)@\(range)"
    }
}
