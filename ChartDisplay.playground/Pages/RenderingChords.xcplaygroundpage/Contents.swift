//: [Previous](@previous)

import AppKit
import PlaygroundSupport

/// Draws the chords of the given chordAuthority above their indexes in the attached NSTextStorage.
class MyLayoutManager: NSLayoutManager {
    
    var chordAuthority: ChordAuthority
    
    init(chordAuthority: ChordAuthority) {
        self.chordAuthority = chordAuthority
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init coder not implemented.")
    }
    
    override func drawGlyphs(forGlyphRange glyphsToShow: NSRange, at origin: NSPoint) {
        super.drawGlyphs(forGlyphRange: glyphsToShow, at: origin)
        
        let charRange = characterRange(forGlyphRange: glyphsToShow, actualGlyphRange: nil)
        for charIndex in charRange.lowerBound..<charRange.upperBound {
            if let chord = chordAuthority.chord(startingAt: charIndex) {
                
                let firstGlyph = glyphIndexForCharacter(at: charIndex)
                // Calculate location for glyph
                let viewPoint = viewLocation(forGlyphAt: firstGlyph)
                // Adjust the chord up.
                let adjustedViewPoint = NSPoint(x: viewPoint.x, y: viewPoint.y - 25)
                chord.textExcludingBrackets.draw(at: adjustedViewPoint)
            }
        }
    }
    
    /// The location in the view this glyph occupys. Accounts for container inset and location in the line fragment.
    func viewLocation(forGlyphAt glyphIndex: Int) -> NSPoint {
        let fragmentRect = lineFragmentRect(forGlyphAt: glyphIndex, effectiveRange: nil)
        let locationInFragment = location(forGlyphAt: glyphIndex)
        guard let containerInset = textContainer(forGlyphAt: glyphIndex, effectiveRange: nil)?.textView?.textContainerInset else {
            fatalError("Could not find textContainerInset. The textContainer or textView for glyphIndex is nil.")
        }
        
        // Offset for line and container.
        return NSPoint(x: containerInset.width + fragmentRect.minX + locationInFragment.x, y: containerInset.height + fragmentRect.minY + locationInFragment.y)
    }
}

/// A NSLayoutManagerDelegate that adds spacing above each line of text.
class ExtraSpacingLMDelegate: NSObject, NSLayoutManagerDelegate {
    
    /// How far to shift the text down.
    var baselineShift: CGFloat
    init(baselineShift: CGFloat) {
        self.baselineShift = baselineShift
    }
    
    /// Adds extra space to the line rect for chords.
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: NSRect) -> CGFloat {
        return baselineShift
    }
    
    /// Implemented to shift the usedRect to only cover the text. The usedRect effects highlighting rects and the cursor size.
    func layoutManager(_ layoutManager: NSLayoutManager, shouldSetLineFragmentRect lineFragmentRect: UnsafeMutablePointer<NSRect>, lineFragmentUsedRect: UnsafeMutablePointer<NSRect>, baselineOffset: UnsafeMutablePointer<CGFloat>, in textContainer: NSTextContainer, forGlyphRange glyphRange: NSRange) -> Bool {
        
        // Push the text down by the baselineShift.
        baselineOffset.pointee += baselineShift
        let proposed = lineFragmentUsedRect.pointee
        
        // Push the line fragment down to only cover text not the extra space created by 'baselineShift'.
        let adjusted = NSRect(x: proposed.minX, y: proposed.minY+baselineShift, width: proposed.width, height: proposed.height-baselineShift)
        lineFragmentUsedRect.pointee = adjusted
        return true
    }
}

/// Informs the NSLayoutManager which glyphs should be hidden because they are chords.
class ChordTextHidingLMDelegate: ExtraSpacingLMDelegate {
    
    var chordAuthority: ChordAuthority
    
    init(baselineShift: CGFloat, chordAuthority: ChordAuthority) {
        self.chordAuthority = chordAuthority
        super.init(baselineShift: baselineShift)
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, shouldGenerateGlyphs glyphs: UnsafePointer<CGGlyph>, properties props: UnsafePointer<NSLayoutManager.GlyphProperty>, characterIndexes charIndexes: UnsafePointer<Int>, font aFont: NSFont, forGlyphRange glyphRange: NSRange) -> Int {
        print("Hiding glyphs")
        glyphRange
        //let newProps: UnsafeMutablePointer<NSLayoutManager.GlyphProperty> = UnsafeMutablePointer<NSLayoutManager.GlyphProperty>.allocate(capacity: glyphRange.length)
        let newProps = UnsafeMutablePointer<NSLayoutManager.GlyphProperty>(mutating: props)
        var foundChord = false
        for index in 0..<glyphRange.length {
            
            let charIndex = charIndexes[index]
            if chordAuthority.chord(at: charIndex) != nil {
                // Hide the glyphs corrosponding to chords.
                newProps[index] = .null
                foundChord = true
            }
        }
        // Change the default glyph properties because the glyphs for a chord need to be hidden.
        if foundChord {
            layoutManager.setGlyphs(glyphs, properties: newProps, characterIndexes: charIndexes, font: aFont, forGlyphRange: glyphRange)
            return glyphRange.length
        } else {
            return 0
        }
    }
}

/// Adopters provide information about the location of text in a TextStorage.
protocol ChordAuthority {
    /// Returns the chord that contains the given index or nil if the given index is not  in a chord.
    func chord(at charIndex: Int) -> Chord?
}
extension ChordAuthority {
    /// Returns the chord who's first character is the given index or nil if the given index is not the first character in a chord.
    func chord(startingAt charIndex: Int) -> Chord? {
        if let chord = chord(at: charIndex), chord.range.lowerBound == charIndex {
            return chord
        } else {
            return nil
        }
    }
    
    /// Returns the chord who's last character is the given index or nil if the given index is not the last character in a chord.
    func chord(endingAt charIndex: Int) -> Chord? {
        if let chord = chord(at: charIndex), chord.range.upperBound == charIndex {
            return chord
        } else {
            return nil
        }
    }
}

/// Takes in a TextStorage and parses it looking for chords it then creates Chord data structures that holds the location of each chord.
class ChartParser: ChordAuthority {
    static let pattern = "\\[[^\\[\\]]*[ABCDEFG][b#]*[m]*(sus)*[2-7]*\\]"
    static let regex: NSRegularExpression = try! NSRegularExpression(pattern: ChartParser.pattern, options: NSRegularExpression.Options(rawValue: 0))
    
    private var notificationCenter: NotificationCenter
    
    var chords = [Chord]()
    var textStorage: NSTextStorage

    /// Listens for changes to any NSText.didChangeNotification that has the same textStorage.
    init(storage: NSTextStorage, notificationCenter: NotificationCenter = .default) {
        self.notificationCenter = notificationCenter
        textStorage = storage
        
        // Observe the textView to parse anytime there is a change.
        notificationCenter.addObserver(forName: NSText.didChangeNotification, object: nil, queue: nil) { [unowned self] notification in
            self.textDidChange(notification)
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    /// Called anytime text changes in the monitored TextView.
    func textDidChange(_ notification: Notification) {
        guard let textView = notification.object as? NSTextView,
            let storage = textView.textStorage,
            storage == self.textStorage else { return }
        storage.beginEditing()
        //Use this once we are using NSTextBlock, storage.range(of: NSTextBlock, at: Int)
        let range = NSRange(location: 0, length: storage.length)
        print("Parsing")
        parse(range)
        storage.edited(.editedAttributes, range: range, changeInLength: 0)
        storage.endEditing()
        chords
    }
    
    /// Parses the given range and updates the Chord values that are stored.
    /// Do not pass a range that has an endpoint that is in the middle of a chord.
    func parse(_ range: NSRange) {
        removeChords(inside: range)
        
        var parsedChords = [Chord]()
        
        // Add matches as a Chord to 'parsedChords'
        ChartParser.regex.enumerateMatches(in: textStorage.string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) { (result, flags, stop) in
            
            guard let result = result else { return }
            let chord = Chord(text: textStorage.attributedSubstring(from: result.range), range: result.range)
            parsedChords.append(chord)
            
            //textStorage.addAttribute(.foregroundColor, value: NSColor.blue, range: result.range)
        }
        
        // This would be a prettier implementation but might result in an extra pass.
//        let results = ChartParser.regex.matches(in: textStorage.string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range)
//        let pChords = results.map { Chord(text: textStorage.attributedSubstring(from: $0.range), range: $0.range) }
        
        chords.append(contentsOf: parsedChords)
        chords.sort { $0.range.lowerBound < $1.range.lowerBound }
    }
    
    /// Returns the chord that contains the given character index, even if the given index is a chord bracket.
    func chord(at charIndex: Int) -> Chord? {
        return chords.first( where: { $0.range.contains(charIndex) })
    }
    
    /// Do not pass a range that has an endpoint that is in the middle of a chord.
    func removeChords(inside removalRange: NSRange) {
        // Lower boundary
        assert(chord(at: removalRange.lowerBound) == chord(startingAt: removalRange.lowerBound), "'removalRange' has a chord the spans the lower boundary.")
        // Upper boundary
        assert(chord(at: removalRange.upperBound) == chord(startingAt: removalRange.upperBound), "'removalRange' has a chord the spans the upper boundary.")
        
        //storage.removeAttribute(.foregroundColor, range: removalRange)
        
        chords.removeAll { (chord) -> Bool in
            // This will remove chords that are partially in the range. But as it stands the parser doesn't parse partial chords. This will create issues if I ever pass in a range that has a chord on the boundary.
            return removalRange.contains(chord.range.upperBound) || removalRange.contains(chord.range.lowerBound)
        }
    }
}

// Set up a parent for two text views.
let parent = NSView(frame: NSRect(x: 0, y: 0, width: 800, height: 400))
PlaygroundPage.current.liveView = parent

let testString = """
You conquered the grave
You crossed the divide
Lost in our sin You made us alive
How can we ever hold it inside
We can't hold back
We're gonna lift You
"""



// Configure TextKit Stack
let storage = NSTextStorage()

let parser = ChartParser(storage: storage)

let leftManager = NSLayoutManager()
let leftDelegate = ExtraSpacingLMDelegate(baselineShift: 10)
leftManager.delegate = leftDelegate

let rightManager = MyLayoutManager(chordAuthority: parser)
let rightDelegate = ChordTextHidingLMDelegate(baselineShift: 10, chordAuthority: parser)
rightManager.delegate = rightDelegate

let leftContainer = NSTextContainer(containerSize: NSSize(width: 400, height: 380))
let rightContainer = NSTextContainer(containerSize: NSSize(width: 400, height: 380))

storage.addLayoutManager(leftManager)
leftManager.addTextContainer(leftContainer)

storage.addLayoutManager(rightManager)
rightManager.addTextContainer(rightContainer)

// Configure Left TextView
let leftView = NSTextView.init(frame: NSRect(x: 0, y: 0, width: 400, height: 380), textContainer: leftContainer)
parent.addSubview(leftView)

// Configure Right TextView
let rightView = NSTextView.init(frame: NSRect(x: 400, y: 0, width: 400, height: 380), textContainer: rightContainer)
parent.addSubview(rightView)

//: [Next](@next)
