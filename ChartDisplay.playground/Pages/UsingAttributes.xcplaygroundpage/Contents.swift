//: A Cocoa based Playground used to demonstrate various techniques needed to render a chord chart.
//: Written by Joseph Van Boxtel, June 16 2018.

import AppKit
import PlaygroundSupport


/// A NSTextBlock subclass that renders the background using a CALayer.
class LayerTextBlock: NSTextBlock {
    
    var layer: CALayer
    
    convenience override init() {
        self.init(layer: CALayer())
    }
    
    init(layer: CALayer) {
        self.layer = layer
        super.init()
        
        setValue(layer.bounds.width, type: .absoluteValueType, for: .width)
        setValue(layer.bounds.height, type: .absoluteValueType, for: .minimumHeight)
        
        setWidth(10, type: .absoluteValueType, for: .padding)
        // Add extra padding to allow chords to hover above. Remember: inverted coordinates.
        setWidth(20, type: .absoluteValueType, for: .padding, edge: .minY)
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let layer = aDecoder.decodeObject(forKey: "Layer") as? CALayer else { return nil }
        self.layer = layer
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(layer, forKey: "Layer")
    }
    
    override func drawBackground(withFrame frameRect: NSRect, in controlView: NSView, characterRange charRange: NSRange, layoutManager: NSLayoutManager) {

        layer.bounds = frameRect
        
        // Render layer into current CGContext.
        guard let context = NSGraphicsContext.current?.cgContext else {
            fatalError("No current graphics context.")
        }
        layer.render(in: context)

        // Allow super to draw border and whatever else over our background.
        super.drawBackground(withFrame: frameRect, in: controlView, characterRange: charRange, layoutManager: layoutManager)
    }
}
//: Set up a text view inside a parent view.
let parent = NSView(frame: NSRect(x: 0, y: 0, width: 400, height: 400))
PlaygroundPage.current.liveView = parent

let leftView = NSTextView(frame: NSRect(x: 0, y: 0, width: 400, height: 380))
parent.addSubview(leftView)

// Pre populate with some text.
leftView.string = """
You conquered the grave
You crossed the divide
Lost in our sin You made us alive
How can we ever hold it inside
We can't hold back
We're gonna lift You
"""
//: Create a helper function to create a paragraph style with a background and the line spacing given.
func paragraphStyle(lineSpacing: CGFloat) -> NSMutableParagraphStyle {
    let style = NSMutableParagraphStyle()
    style.lineSpacing = lineSpacing;
    
    // Create a background layer for the paragraph.
    let boxSize = NSRect(x: 0, y: 0, width: 200, height: 40)
    let circleLayer = CALayer()
    circleLayer.bounds = boxSize
    circleLayer.cornerRadius = lineSpacing
    circleLayer.backgroundColor = NSColor.yellow.cgColor
    
    let layerBlock = LayerTextBlock(layer: circleLayer)
    style.textBlocks = [layerBlock]
    
    return style
}
//: Extend TextView to allow us to update the paragraph style more easily.
extension NSTextView {
    ///Set the typing paragraph style and set it on the whole string.
    func updateParagraphStyle(_ paragraphStyle: NSParagraphStyle) {
        typingAttributes[.paragraphStyle] = paragraphStyle
        textStorage!.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: textStorage!.length))
    }
}
//: Create a class that will serve as the target for the slider.
class BasicController: NSObject {
    
    var textView: NSTextView
    init(textView: NSTextView) {
        self.textView = textView
    }
    
    // Slider value is expected to be between 0.0 and 1.0
    @objc func sliderChanged(slider: NSSlider) {
        let newValue = CGFloat(slider.doubleValue)
        textView.updateParagraphStyle(paragraphStyle(lineSpacing: newValue*50))
    }
}



//: Wire up the slider to control line spacing.
let callback = { (slider: NSSlider) in
    let newValue = CGFloat(slider.doubleValue)
    leftView.updateParagraphStyle(paragraphStyle(lineSpacing: newValue*50.0))
}
//let buttonSlider = ClosureSlider(closure: callback, value: 1, minValue: 0, maxValue: 1)
let buttonSlider = ClosureSlider(closure: callback, value: 1.0, minValue: 0.0, maxValue: 1.0)
parent.addSubview(buttonSlider)
//buttonSlider.doubleValue = 0.5
leftView.updateParagraphStyle(paragraphStyle(lineSpacing: 25))

//: Notice when you run the the cursor and any selected text acts as if the text takes up the whole space for each line even though text only takes up part of it.
