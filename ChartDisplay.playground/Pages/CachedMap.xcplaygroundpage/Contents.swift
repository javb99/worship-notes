//: [Previous](@previous)

import Foundation

extension Sequence {
    /// Transforms each element in the sequence using the given transform while avoiding using a cache with the index as the key. The transform is not used for indexes that have a transformed element in the cache.
    func map<T>(usingCache cache: inout [Int: T], transform: (Element) throws -> T) -> [T] {
        var transformed = [T]()
        for (i, element) in enumerated() {
            if let transformedElement = cache[i] {
                transformed.append(transformedElement)
            } else {
                let transformedElement = transform(element)
                cache[i] = transformedElement
                transformed.append(transformedElement)
            }
        }
        return transformed
    }
}

var cache = [Int: Int]()
var data = [10, 5, 0]
let mappedA = data.map(usingCache: &cache) { $0 * 10 }
mappedA
data.append(7)
let mappedB = data.map(usingCache: &cache) { $0 * 10 }
mappedB
let mappedC = data.map(usingCache: &cache) { $0 * 10 }
mappedC

//: [Next](@next)
