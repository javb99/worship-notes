//: [Previous](@previous)

import AppKit

let pattern = "\\[[^\\[\\]]*[ABCDEFG][b#]*[m]*(sus)*[2-7]*\\]"
//let starPattern = "\\*[^\\*\\*]*[ABCDEFG][b#]*[m]*(sus)*[2-7]*\\*"

let testInput = """
You conquered the [*G*]grave
You [F##]crossed the divide
Lost in our [B#m]sin You [Dsus4]made us alive
How can we ever[]B] hold[B]] it inside
We [Gbm]can't [Am7]hold back
We're gonna lift You
"""

let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))

var allMatches = ""

let fullRange = NSRange((testInput.startIndex..<testInput.endIndex), in: testInput)
regex.enumerateMatches(in: testInput, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: fullRange) { (result, flag, stop) in
    let range = Range(result!.range, in: testInput)!
    allMatches += testInput[range]
}
allMatches

/// Represents a chord in a string that has been parsed.
struct Chord {
    /// The text for the chord, excluding the brackets.
    var text: String
    
    /// The character range this chord occupies in the parsed string.
    var range: NSRange
    
    var textExcludingBrackets: String {
        let excludingBrackets = NSRange(location: 1, length: range.length-2)
        return String(text[Range(excludingBrackets, in: text)!])
    }
    
    /// The character range that this chord occupies excluding the brackets('[' and ']').
    var rangeExcludingBrackets: NSRange {
        return NSRange(location: range.location-1, length: range.length-2)
    }
}

extension Chord: CustomPlaygroundDisplayConvertible {
    var playgroundDescription: Any {
        return "\(text)@\(range)"
    }
}

extension NSRange {
    var min: Int {
        return location
    }
    var max: Int {
        return location+length
    }
}

/// Takes in a TextStorage and parses it looking for chords it then creates Chord data structures that holds the location of each chord.
class ChartParser {
    static let pattern = "\\[[^\\[\\]]*[ABCDEFG][b#]*[m]*(sus)*[2-7]*\\]"
    let regex: NSRegularExpression = try! NSRegularExpression(pattern: ChartParser.pattern, options: NSRegularExpression.Options(rawValue: 0))
    
    var chords = [Chord]()
    
    // Parses the given range and updates the Chord values that are stored.
    // Do not pass a range that has an endpoint that is in the middle of a chord.
    func parse(_ range: NSRange, of string: String) {
        removeChords(inside: range)
        
        var parsedChords = [Chord]()
        regex.enumerateMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) { (result, flags, stop) in
            
            guard let result = result else { return }
            let swiftRange = Range(result.range, in: string)!
            let chord = Chord(text: String(string[swiftRange]), range: result.range)
            parsedChords.append(chord)
        }
        
        chords.append(contentsOf: parsedChords)
        chords.sort { $0.range.min < $1.range.min }
    }
    
    /// Returns the chord that contains the given character index, even if the given index is a chord bracket.
    func chord(at charIndex: Int) -> Chord? {
        for chord in chords {
            // If we pass it, we can stop.
            if chord.range.min > charIndex {
                return nil
            }
            
            if chord.range.contains(charIndex) {
                return chord
            }
        }
        return nil
    }
    
    func removeChords(inside removalRange: NSRange) {
        chords.removeAll { (chord) -> Bool in
            // This will remove chords that are partially in the range. But as it stands the parser doesn't parse partial chords. This will create issues if I ever pass in a range that has a chord on the boundary.
            return removalRange.contains(chord.range.max) || removalRange.contains(chord.range.min)
        }
    }
}

let parser = ChartParser()
let swiftRange = testInput.startIndex..<testInput.endIndex
parser.parse(NSRange(swiftRange, in: testInput), of: testInput)
parser.chords
parser.parse(NSRange(swiftRange, in: testInput), of: testInput)
parser.chords
//: [Next](@next)
