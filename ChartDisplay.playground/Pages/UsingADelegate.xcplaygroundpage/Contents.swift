//: [Previous](@previous)

import AppKit
import PlaygroundSupport

class MyLayoutManager: NSLayoutManager {
    
    override func drawGlyphs(forGlyphRange glyphsToShow: NSRange, at origin: NSPoint) {
        super.drawGlyphs(forGlyphRange: glyphsToShow, at: origin)
        
        let charRange = characterRange(forGlyphRange: glyphsToShow, actualGlyphRange: nil)
        storage.enumerateAttribute(.foregroundColor, in: charRange, options: NSAttributedString.EnumerationOptions(rawValue: 0)) { (value, overRange, stop) in
            
            // Check for chord
            guard let color = value as? NSColor, color != .black else { return }
            
            let chordString = storage.attributedSubstring(from: overRange)
            let overGlyphRange = glyphRange(forCharacterRange: overRange, actualCharacterRange: nil)
            
            // Calculate location for glyph
            let fragmentRect = lineFragmentRect(forGlyphAt: overGlyphRange.location, effectiveRange: nil)
            let expectedGlyphLocation = location(forGlyphAt: overGlyphRange.location)
            let containerInset = textContainer(forGlyphAt: overGlyphRange.location
                , effectiveRange: nil)!.textView!.textContainerInset
            
            // Offset for line and container then shift up.
            let actualLocation = NSPoint(x: containerInset.width + fragmentRect.minX + expectedGlyphLocation.x, y: containerInset.height + fragmentRect.minY + expectedGlyphLocation.y - 25)
            chordString.draw(at: actualLocation)
        }
    }
    
    override func drawBackground(forGlyphRange glyphsToShow: NSRange, at origin: NSPoint) {
        super.drawBackground(forGlyphRange: glyphsToShow, at: origin)
    }
}

class MyLayoutManagerDelegate: NSObject, NSLayoutManagerDelegate {
    
    var baselineShift: CGFloat
    init(baselineShift: CGFloat) {
        self.baselineShift = baselineShift
    }
    
    // Adds extra space to the line rect for chords.
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: NSRect) -> CGFloat {
        return baselineShift
    }
    
    // Implemented to shift the usedRect to only cover the text. The usedRect effects highlighting rects and the cursor size.
    func layoutManager(_ layoutManager: NSLayoutManager, shouldSetLineFragmentRect lineFragmentRect: UnsafeMutablePointer<NSRect>, lineFragmentUsedRect: UnsafeMutablePointer<NSRect>, baselineOffset: UnsafeMutablePointer<CGFloat>, in textContainer: NSTextContainer, forGlyphRange glyphRange: NSRange) -> Bool {
        
        // Push the text down by the baselineShift.
        baselineOffset.pointee += baselineShift
        let proposed = lineFragmentUsedRect.pointee
        
        // Push the line fragment down to only cover text not the extra space created by baselineShift.
        let adjusted = NSRect(x: proposed.minX, y: proposed.minY+baselineShift, width: proposed.width, height: proposed.height-baselineShift)
        lineFragmentUsedRect.pointee = adjusted
        return true
    }
    
    func isChord(charIndex: Int) -> Bool {
        if let foregroundColor = storage.attribute(.foregroundColor, at: charIndex, effectiveRange: nil) as? NSColor {
            return foregroundColor == NSColor.blue
        }
        return false
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, shouldGenerateGlyphs glyphs: UnsafePointer<CGGlyph>, properties props: UnsafePointer<NSLayoutManager.GlyphProperty>, characterIndexes charIndexes: UnsafePointer<Int>, font aFont: NSFont, forGlyphRange glyphRange: NSRange) -> Int {
        
        var newProps: UnsafeMutablePointer<NSLayoutManager.GlyphProperty> = UnsafeMutablePointer<NSLayoutManager.GlyphProperty>.allocate(capacity: glyphRange.length)
        
        var foundChord = false
        for index in 0..<glyphRange.length {
            
            let charIndex = charIndexes[index]
            if isChord(charIndex: charIndex) {
                newProps[index] = .null
                foundChord = true
            }
        }
        if foundChord {
            layoutManager.setGlyphs(glyphs, properties: newProps, characterIndexes: charIndexes, font: aFont, forGlyphRange: glyphRange)
            return glyphRange.length
        } else {
            return 0
        }
    }
}

//: I was having issues with the cursor jumping until reading this blog post. Lots of information on the editing cycle. [Blog Post](https://christiantietze.de/posts/2017/11/syntax-highlight-nstextstorage-insertion-point-change/) This article suggests monitoring for changes using NSText.didChangeNotification or NSTextDelegate.
class SyntaxHighlighter: NSObject {
    
    private var notificationCenter: NotificationCenter
    
    init(textView: NSTextView, notificationCenter: NotificationCenter = .default) {
        self.notificationCenter = notificationCenter
        super.init()
        notificationCenter.addObserver(forName: NSText.didChangeNotification, object: textView, queue: nil) { [unowned self] notification in
            self.textDidChange(notification)
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
   
    static let regex: NSRegularExpression = {
        //let pattern = "And"
        let pattern = "[ABCDEFG][#|b]*"
        return try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
    }()
    
    func applySyntaxHighlights(to textStorage: NSTextStorage, editedRange: NSRange) {
        let string = textStorage.string
        
        // Expand edit range to whole paragraph.
        let swiftEditRange = Range(editedRange, in: string)!
        let paragraphRange = string.paragraphRange(for: swiftEditRange)
        let nsParagraphRange = NSRange(paragraphRange, in: string)
        
        // Clear past formatting.
        textStorage.removeAttribute(.foregroundColor, range: nsParagraphRange)
        timeCode {
            // Add attribute to each match.
            SyntaxHighlighter.regex.enumerateMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: nsParagraphRange) { (result, flags, stop) in
                textStorage.addAttribute(.foregroundColor, value: NSColor.blue, range: result!.range)
                //textStorage.addAttribute(.superscript, value: 10, range: result!.range)
            }
        }
    }
    
    func textDidChange(_ notification: Notification) {
        let textView = notification.object as! NSTextView
        let storage = textView.textStorage!
        storage.beginEditing()
        applySyntaxHighlights(to: storage, editedRange: NSRange(location: 0, length: storage.length))
        storage.endEditing()
    }
}

//: Set up a text view inside a parent view.
let parent = NSView(frame: NSRect(x: 0, y: 0, width: 800, height: 400))
PlaygroundPage.current.liveView = parent

let testString = """
You conquered the grave
You crossed the divide
Lost in our sin You made us alive
How can we ever hold it inside
We can't hold back
We're gonna lift You
"""

// Configure TextKit Stack
let storage = NSTextStorage()
let leftManager = MyLayoutManager()
let leftDelegate = MyLayoutManagerDelegate(baselineShift: 10)
leftManager.delegate = leftDelegate
let rightManager = MyLayoutManager()
let rightDelegate = MyLayoutManagerDelegate(baselineShift: 10)
rightManager.delegate = rightDelegate

let leftContainer = NSTextContainer(containerSize: NSSize(width: 400, height: 380))
let rightContainer = NSTextContainer(containerSize: NSSize(width: 400, height: 380))

storage.addLayoutManager(leftManager)
leftManager.addTextContainer(leftContainer)

storage.addLayoutManager(rightManager)
rightManager.addTextContainer(rightContainer)

// Configure Left TextView
let leftView = NSTextView.init(frame: NSRect(x: 0, y: 0, width: 400, height: 380), textContainer: leftContainer)
leftView.textContainer?.replaceLayoutManager(leftManager)
parent.addSubview(leftView)

// Configure Right TextView
let rightView = NSTextView.init(frame: NSRect(x: 400, y: 0, width: 400, height: 380), textContainer: rightContainer)
rightView.textContainer?.replaceLayoutManager(rightManager)
parent.addSubview(rightView)

let highlighter = SyntaxHighlighter(textView: rightView)

// Configure Slider
let callback: (NSSlider)->() = { (slider: NSSlider) in
    let newValue = slider.doubleValue*20.0
    leftDelegate.baselineShift = CGFloat(CGFloat(newValue))
    leftManager.invalidateLayout(forCharacterRange: NSRange(location: 0, length: storage.length), actualCharacterRange: nil)
    rightDelegate.baselineShift = CGFloat(CGFloat(newValue))
    rightManager.invalidateLayout(forCharacterRange: NSRange(location: 0, length: storage.length), actualCharacterRange: nil)
}
let buttonSlider = ClosureSlider(closure: callback, value: 0.5, minValue: 0.0, maxValue: 1.0)
parent.addSubview(buttonSlider)

//: [Next](@next)
