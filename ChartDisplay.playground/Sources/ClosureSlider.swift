import Cocoa

public class ClosureSlider: NSSlider {
    var callback: (NSSlider)->()
    public init(closure: @escaping (NSSlider)->(), value: Double = 1, minValue: Double = 0, maxValue: Double = 1) {
        callback = closure
        super.init(frame: NSRect(x: 0, y: 0, width: 100, height: 40))
        self.doubleValue = value
        self.minValue = minValue
        self.maxValue = maxValue
        target = self
        action = #selector(ClosureSlider.changedSliderValue)
        //super.init(value: value, minValue: minValue, maxValue: maxValue, target: self, action: #selector(ClosureSlider.changedSliderValue))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func changedSliderValue() {
        callback(self)
    }
}
